#!/bin/bash

glslangValidator -V fullscreen_quad.frag -o fullscreen_quad.frag.spv
glslangValidator -V fullscreen_quad.vert -o fullscreen_quad.vert.spv

glslangValidator -V simpleTriangle.frag -o frag.spv
glslangValidator -V simpleTriangle.vert -o vert.spv
