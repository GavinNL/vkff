#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(set = 0, binding = 0) uniform sampler2D TEXTURE[1];

//========================================================================
// Input attributes.
//========================================================================
layout(location = 0) in vec4 f_POSITION;
layout(location = 1) in vec2 f_TEXCOORD_0;


//========================================================================
// Output Color
//========================================================================
layout(location = 0) out vec4 out_Color;


//========================================================================
// Push constants. This should be in all stages.
//========================================================================
layout(push_constant) uniform PushConsts {
        vec2 position;
        vec2 size;
        vec2 screenSize;
        vec2 unused;
} pushConsts;

const float PI = 3.14159265359;


void main()
{
   float dx = 5.0 / pushConsts.screenSize.x;
   float dy = 5.0 / pushConsts.screenSize.y;


vec3 norm = (
texture( TEXTURE[0] , f_TEXCOORD_0.xy + vec2(0, 0)  ) .rgb +
texture( TEXTURE[0] , f_TEXCOORD_0.xy + vec2(dx, 0)  ) .rgb +
texture( TEXTURE[0] , f_TEXCOORD_0.xy + vec2(-dx, 0)  ) .rgb +
texture( TEXTURE[0] , f_TEXCOORD_0.xy + vec2(0, dy)  ) .rgb +
texture( TEXTURE[0] , f_TEXCOORD_0.xy + vec2(0, -dy)  ) .rgb ) * 0.2;
   out_Color = vec4( norm,1);
}

