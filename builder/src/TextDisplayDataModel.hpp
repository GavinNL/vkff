#pragma once

#include <QtCore/QObject>
#include <QtWidgets/QLabel>
#include <QSpinBox>
#include <QLineEdit>
#include <QJsonDocument>
//#include "TextData.hpp"

#include <nodes/NodeDataModel>
#include <nodes/NodeStyle>
#include <nodes/FlowViewStyle>
#include <nodes/ConnectionStyle>
#include <nodes/Connection>
#include <nodes/Node>
#include <qjform.h>
#include <iostream>

#include "framegraph/defines.h"
/// The model dictates the number of inputs and outputs for the Node.
/// In this example it has no logic.


namespace FGQ
{
    using QtNodes::PortType;
    using QtNodes::PortIndex;
    using QtNodes::NodeData;
    using QtNodes::NodeDataModel;
    using QtNodes::NodeDataType;

    // Render Pass Data
    const QString RenderPassInputTextureID    = "InputTextureID";
    const QString RenderPassColorAttachmentID = "ColorAttachmentID";
    const QString RenderPassDepthAttachmentID = "DepthAttachmentID";

    // Color Attachment Data
    const QString TextureColorDataID    = "TextureColorDataID";
    const QString TextureDepthDataID    = "TextureDepthDataID";

    const QString SampledImageDataID = "SampledImageDataID";
    const QString SampledImageID = "SampledImageID";

    const NodeDataType DataNull{ "nullData", "Null"};


    class FGQBase : public NodeDataModel
    {
      Q_OBJECT

    protected:
        QString m_caption;

        struct Data_t
        {
            QString name = "null_node";
            QString caption = "NullNode";
            std::vector<NodeDataType> inNodes;
            std::vector<NodeDataType> outNodes;
            QJForm::QJForm * m_widget=nullptr;
            bool resizable = true;

            QtNodes::NodeValidationState validationState = QtNodes::NodeValidationState::Valid;
            QString validationMessage;
        };

        Data_t m_data;
    public:
      FGQBase()
      {
            //m_data.outNodes = { RawImageOut };
//            m_data.m_widget = new QJForm::QJForm();
//            m_data.m_widget->setSchema(
//                        ObjectFromString(
//                            R"foo(
//{
//    "type" : "string"
//}
//                            )foo"
//                        ));
      }
      virtual ~FGQBase()
      {
      }



      QtNodes::NodeValidationState validationState() const override
      {
          return m_data.validationState;
      }

      QString validationMessage() const override
      {
          return m_data.validationMessage;
      }


      QJsonObject save() const override
      {
          auto J = NodeDataModel::save();

          if( m_data.m_widget)
              J["frameGraph"] = m_data.m_widget->get();

          return J;
      }
      //
      ////
      void restore(QJsonObject const &p) override
      {
          NodeDataModel::restore(p);
          if( m_data.m_widget)
          {
              if( p.contains("frameGraph"))
              {
                  m_data.m_widget->setValue( p.find("frameGraph")->toObject() );
              }
          }
          (void)p;
          //    return m_data.m_widget->set
      }

      static QJsonObject ObjectFromString(const QString& in)
      {
          QJsonObject obj;

          QJsonDocument doc = QJsonDocument::fromJson(in.toUtf8());

          // check validity of the document
          if(!doc.isNull())
          {
              if(doc.isObject())
              {
                  obj = doc.object();
              }
              else
              {
                  return {};
              }
          }
          else
          {
              return {};
          }

          return obj;
      }

    public:

      bool resizable() const override
      {
          return m_data.resizable;
      }

      QString caption() const override
      {
          return m_data.caption;
      }

      bool captionVisible() const override
      {
          return true;
      }


      QString name() const override
      {
          return m_data.name;
      }

    public:

      unsigned int nPorts(PortType portType) const override
      {
          switch (portType)
          {
            case PortType::In:
              return static_cast<unsigned int>( m_data.inNodes.size() );
              break;

            case PortType::Out:
              return static_cast<unsigned int>( m_data.outNodes.size() );
            default:
              break;
          }
          return 0;
      }

      NodeDataType dataType(PortType portType, PortIndex portIndex) const override
      {
          switch (portType)
          {
            case PortType::In:
              return m_data.inNodes.at(portIndex);
              break;

            case PortType::Out:
              return m_data.outNodes.at(portIndex);
            default:
              break;
          }
          return DataNull;
      }

      std::shared_ptr<NodeData> outData(PortIndex port) override
      {
          (void)port;
          std::shared_ptr<NodeData> ptr;
          return ptr;
      }

      void setInData(std::shared_ptr<NodeData> data, int) override
      {
            return;
      }

      QWidget * embeddedWidget() override
      {
          return m_data.m_widget;
      }


      virtual void
      inputConnectionCreated(QtNodes::Connection const& c) override
      {
          std::cout << c.getNode(PortType::In)->nodeDataModel()->name().toStdString() << " ---- ";
          std::cout << c.getNode(PortType::Out)->nodeDataModel()->name().toStdString() << std::endl;
          //c.getPortIndex(PortType::In)
          //  c.getPortIndex(PortType::Out)
          //c.getNode()
          //std::cout << "In connect: " << this->name().toStdString() << std::endl;;

      }

      virtual void
      inputConnectionDeleted(QtNodes::Connection  const&) override
      {
      }

      virtual void
      outputConnectionCreated(QtNodes::Connection  const&) override
      {
      }

      virtual void
      outputConnectionDeleted(QtNodes::Connection  const&) override
      {
      }

    private:
    };


    class NodeSwapchainImage : public FGQBase
    {
        Q_OBJECT
    public:
        NodeSwapchainImage()
        {
              //m_data.outNodes = { RawImageOut };
              m_data.name = "NodeSwapchainImage";
              m_data.caption = "Swapchain Image";
              m_data.outNodes = { {TextureColorDataID, "Texture Data"} };
#if 0
              m_data.m_widget = new QJForm::QJForm();
              m_data.m_widget->setSchema(
                          ObjectFromString(
                              R"foo(
{
    "type" : "object",
    "properties" : {
      "name" : {
          "type" : "string"
      },
      "format" : {
          "type" : "string",
          "enum" : ["R8G8B8A8Unorm", "R32G32B32A32Sfloat"]
      }
    },
    "ui:order" : ["name", "format"]
}
                              )foo"
                          ));
              m_data.m_widget->setStyleSheet("background-color: rgba(0,0,0,0)");
#endif
              QtNodes::NodeStyle style(
              R"(
              {
              "NodeStyle": {
                "NormalBoundaryColor": [255, 255, 0],
                "SelectedBoundaryColor": [255, 165, 0],
                "GradientColor0": "gray",
                "GradientColor1": [160, 160, 160],
                "GradientColor2": [128, 128, 128],
                "GradientColor3": [116, 116, 116],
                "ShadowColor": [20, 20, 20],
                "FontColor" : "white",
                "FontColorFaded" : [10, 10, 10],
                "ConnectionPointColor": [169, 169, 169],
                "FilledConnectionPointColor": "cyan",
                "ErrorColor": "red",
                "WarningColor": [128, 128, 0],

                "PenWidth": 2.0,
                "HoveredPenWidth": 4.5,

                "ConnectionPointDiameter": 8.0,

                "Opacity": 0.8
              }
              }
              )");
              setNodeStyle( style );
        }
        virtual ~NodeSwapchainImage()
        {
        }
    };

    class NodePhysicalColorImage : public FGQBase
    {
        Q_OBJECT
    public:
        NodePhysicalColorImage()
        {
              //m_data.outNodes = { RawImageOut };
              m_data.name     = COLOR_IMAGE_NAME_STR;
              m_data.caption  = "Physical Color Image";
              m_data.outNodes = { {TextureColorDataID, "Texture Data"} };
              m_data.m_widget = new QJForm::QJForm();
              m_data.m_widget->setSchema(
                          ObjectFromString(
                              R"foo(
{
    "type" : "object",
    "properties" : {
      "name" : {
          "type" : "string"
      },
      "format" : {
          "type" : "string",
          "enum" : ["R8G8B8A8Unorm", "R32G32B32A32Sfloat"]
      }
    },
    "ui:order" : ["name", "format"]
}
                              )foo"
                          ));
              m_data.m_widget->setStyleSheet("background-color: rgba(0,0,0,0)");


              QtNodes::NodeStyle style(
              R"(
              {
              "NodeStyle": {
                "NormalBoundaryColor": [255, 255, 0],
                "SelectedBoundaryColor": [255, 165, 0],
                "GradientColor0": "gray",
                "GradientColor1": [80, 80, 80],
                "GradientColor2": [64, 64, 64],
                "GradientColor3": [58, 58, 58],
                "ShadowColor": [20, 20, 20],
                "FontColor" : "white",
                "FontColorFaded" : "gray",
                "ConnectionPointColor": [169, 169, 169],
                "FilledConnectionPointColor": "cyan",
                "ErrorColor": "red",
                "WarningColor": [128, 128, 0],

                "PenWidth": 2.0,
                "HoveredPenWidth": 4.5,

                "ConnectionPointDiameter": 8.0,

                "Opacity": 0.8
              }
              }
              )");
              setNodeStyle( style );
        }
        virtual ~NodePhysicalColorImage()
        {
        }
    };

    class NodeDepthStencilImage : public FGQBase
    {
        Q_OBJECT
    public:
        NodeDepthStencilImage()
        {
              //m_data.outNodes = { RawImageOut };
              m_data.name     = DEPTH_IMAGE_NAME_STR;
              m_data.caption  = "Physical DepthStencil Image";
              m_data.outNodes = { {TextureDepthDataID, "Depth Data"} };
              m_data.m_widget = new QJForm::QJForm();
              m_data.m_widget->setSchema(
                          ObjectFromString(
                              R"foo(
{
    "type" : "object",
    "properties" : {
      "name" : {
          "type" : "string"
      },
      "format" : {
          "type" : "string",
          "enum" : ["D32Sfloat", "D24UnormS8Uint", "D32SfloatS8Uint", "eD16Unorm", "eD16UnormS8Uint" ]
      }
    },
    "ui:order" : ["name", "format"]
}
                              )foo"
                          ));
              m_data.m_widget->setStyleSheet("background-color: rgba(0,0,0,0)");


              QtNodes::NodeStyle style(
              R"(
              {
              "NodeStyle": {
                "NormalBoundaryColor": [255, 255, 0],
                "SelectedBoundaryColor": [255, 165, 0],
                "GradientColor0": "gray",
                "GradientColor1": [80, 80, 80],
                "GradientColor2": [64, 64, 64],
                "GradientColor3": [58, 58, 58],
                "ShadowColor": [20, 20, 20],
                "FontColor" : "white",
                "FontColorFaded" : "gray",
                "ConnectionPointColor": [169, 169, 169],
                "FilledConnectionPointColor": "cyan",
                "ErrorColor": "red",
                "WarningColor": [128, 128, 0],

                "PenWidth": 2.0,
                "HoveredPenWidth": 4.5,

                "ConnectionPointDiameter": 8.0,

                "Opacity": 0.8
              }
              }
              )");
              setNodeStyle( style );
        }
        virtual ~NodeDepthStencilImage()
        {
        }
    };

    class NodeInputTexture : public FGQBase
    {
        Q_OBJECT
    public:
        NodeInputTexture()
        {
              //m_data.outNodes = { RawImageOut };
              m_data.name = "NodeInputTexture";
              m_data.caption = "Input Texture";
              m_data.inNodes  = { {TextureColorDataID, "Texture Data" } };
              m_data.outNodes = { {TextureColorDataID, "Texture Data" } };
              m_data.m_widget = new QJForm::QJForm();
              m_data.m_widget->setSchema(
                          ObjectFromString(
                              R"foo(
{
    "type" : "object",
    "properties" : {
      "name" : {
          "type" : "string"
      }
    },
    "ui:order" : ["name"]
}
                              )foo"
                          ));
              m_data.m_widget->setStyleSheet("background-color: rgba(0,0,0,0)");
        }
        virtual ~NodeInputTexture()
        {
        }
        virtual void restore(const QJsonObject &p) override
        {
        }
    };

    class NodeInputSampledImage : public FGQBase
    {
        Q_OBJECT
    public:
        NodeInputSampledImage()
        {
              //m_data.outNodes = { RawImageOut };
              m_data.name = SAMPLED_IMAGE_NAME_STR;
              m_data.caption = "Sampled Image";
              m_data.inNodes  = { {SampledImageDataID, "Sampled Data" } };
              m_data.outNodes = { {SampledImageID, "Sampled Image" } };
              m_data.resizable =false;
#if 0
              m_data.m_widget = new QJForm::QJForm();
              m_data.m_widget->setSchema(
                          ObjectFromString(
                              R"foo(
{
    "type" : "object",
    "properties" : {
      "name" : {
          "type" : "string"
      }
    },
    "ui:order" : ["name"]
}
                              )foo"
                          ));
#endif
            _updateValidation();
        }
        virtual ~NodeInputSampledImage()
        {
        }

        bool inputConnect=false;
        bool outputConnect=false;

        virtual void inputConnectionCreated(QtNodes::Connection const& c) override
        {
            inputConnect = true;
            _updateValidation();
        }
        virtual void inputConnectionDeleted(QtNodes::Connection const& c) override
        {
            inputConnect = false;
            _updateValidation();
        }
        virtual void outputConnectionCreated(QtNodes::Connection const& c) override
        {
            outputConnect = true;
            _updateValidation();
        }
        virtual void outputConnectionDeleted(QtNodes::Connection const& c) override
        {
            outputConnect = false;
            _updateValidation();
        }
        void _updateValidation()
        {
            if( inputConnect && outputConnect)
            {
                this->m_data.validationState = QtNodes::NodeValidationState::Valid;
            }
            else if( inputConnect && !outputConnect)
            {
                this->m_data.validationState = QtNodes::NodeValidationState::Warning;
                this->m_data.validationMessage = "Output Not connected";
            }
            else if( !inputConnect )
            {
                this->m_data.validationState = QtNodes::NodeValidationState::Error;
                this->m_data.validationMessage = "Input Must be connected to the output of an Attachment";
            }
        }
    };

    class NodeColorAttachment : public FGQBase
    {
        Q_OBJECT
    public:
        NodeColorAttachment()
        {
              //m_data.outNodes = { RawImageOut };
              m_data.name   = COLOR_ATTACHMENT_NAME_STR;
              m_data.caption = "Color Attachment";
              m_data.inNodes  = { {TextureColorDataID ,"Texture Data"}, { RenderPassColorAttachmentID, "Color Attachment" } };
              m_data.outNodes = { {SampledImageDataID, "Sampled Data"} };
              m_data.m_widget = new QJForm::QJForm();
              m_data.m_widget->setSchema(
                          ObjectFromString(
                              R"foo(
{
    "type" : "object",
    "properties" : {
      "name" : {
          "type" : "string"
      },
      "clearColor" : {
          "type" : "string"
      }
    },
    "ui:order" : ["name", "clearColor"]
}
                              )foo"
                          ));
              m_data.m_widget->setStyleSheet("background-color: rgba(0,0,0,0)");
              _updateValidation();
        }
        virtual ~NodeColorAttachment()
        {
        }


        bool dataConnected=false;
        bool writerConnected=false;
        bool outputConnected=false;
        virtual void inputConnectionCreated(QtNodes::Connection const& c) override
        {
            switch( c.getPortIndex( PortType::In) )
            {
                case 0: dataConnected=true; break;
                case 1: writerConnected=true; break;
                default:
                    break;
            }
            _updateValidation();
        }
        virtual void inputConnectionDeleted(QtNodes::Connection const& c) override
        {
            switch( c.getPortIndex( PortType::In) )
            {
                case 0: dataConnected=false; break;
                case 1: writerConnected=false; break;
                default:
                    break;
            }
            _updateValidation();
        }
        virtual void outputConnectionCreated(QtNodes::Connection const& c) override
        {
            switch( c.getPortIndex( PortType::Out) )
            {
                case 0: outputConnected=true; break;
                default:
                    break;
            }
            _updateValidation();
        }
        virtual void outputConnectionDeleted(QtNodes::Connection const& c) override
        {
            switch( c.getPortIndex( PortType::Out) )
            {
                case 0: outputConnected=false; break;
                default:
                    break;
            }
            _updateValidation();
        }

        void _updateValidation()
        {
            if( dataConnected && writerConnected)
            {
                this->m_data.validationState = QtNodes::NodeValidationState::Valid;
            }
            else
            {
               if( !dataConnected && !writerConnected)
               {
                   this->m_data.validationState = QtNodes::NodeValidationState::Error;
                   this->m_data.validationMessage = "Not connected";
               }
               else
               {
                   if( !dataConnected )
                   {
                       this->m_data.validationState = QtNodes::NodeValidationState::Error;
                       this->m_data.validationMessage = "No physical data";
                   }
                   else if( !writerConnected )
                   {
                       this->m_data.validationState = QtNodes::NodeValidationState::Error;
                       this->m_data.validationMessage = "Nothing writes to this attachments";
                   }
               }
            }
        }

    };

    class NodeDepthStencilAttachment : public FGQBase
    {
        Q_OBJECT
    public:
        NodeDepthStencilAttachment()
        {
              //m_data.outNodes = { RawImageOut };
              m_data.name   = DEPTH_ATTACHMENT_NAME_STR;
              m_data.caption = "DepthStencil Attachment";
              m_data.inNodes  = { {TextureDepthDataID ,"Depth Data"}, { RenderPassDepthAttachmentID, "DepthStencil Attachment" } };
              m_data.outNodes = { {SampledImageDataID, "Sampled Data"} };
              m_data.m_widget = new QJForm::QJForm();
              m_data.m_widget->setSchema(
                          ObjectFromString(
                              R"foo(
{
    "type" : "object",
    "properties" : {
      "name" : {
          "type" : "string"
      },
      "clearColor" : {
          "type" : "string"
      }
    },
    "ui:order" : ["name", "clearColor"]
}
                              )foo"
                          ));
              m_data.m_widget->setStyleSheet("background-color: rgba(0,0,0,0)");


        }
        virtual ~NodeDepthStencilAttachment()
        {
        }
        bool dataConnected=false;
        bool writerConnected=false;
        bool outputConnected=false;
        virtual void inputConnectionCreated(QtNodes::Connection const& c) override
        {
            switch( c.getPortIndex( PortType::In) )
            {
                case 0: dataConnected=true; break;
                case 1: writerConnected=true; break;
                default:
                    break;
            }
            _updateValidation();
        }
        virtual void inputConnectionDeleted(QtNodes::Connection const& c) override
        {
            switch( c.getPortIndex( PortType::In) )
            {
                case 0: dataConnected=false; break;
                case 1: writerConnected=false; break;
                default:
                    break;
            }
            _updateValidation();
        }
        virtual void outputConnectionCreated(QtNodes::Connection const& c) override
        {
            switch( c.getPortIndex( PortType::Out) )
            {
                case 0: outputConnected=true; break;
                default:
                    break;
            }
            _updateValidation();
        }
        virtual void outputConnectionDeleted(QtNodes::Connection const& c) override
        {
            switch( c.getPortIndex( PortType::Out) )
            {
                case 0: outputConnected=false; break;
                default:
                    break;
            }
            _updateValidation();
        }

        void _updateValidation()
        {
            if( dataConnected && writerConnected)
            {
                this->m_data.validationState = QtNodes::NodeValidationState::Valid;
            }
            else
            {
               if( !dataConnected && !writerConnected)
               {
                   this->m_data.validationState = QtNodes::NodeValidationState::Error;
                   this->m_data.validationMessage = "Not connected";
               }
               else
               {
                   if( !dataConnected )
                   {
                       this->m_data.validationState = QtNodes::NodeValidationState::Error;
                       this->m_data.validationMessage = "No physical data";
                   }
                   else if( !writerConnected )
                   {
                       this->m_data.validationState = QtNodes::NodeValidationState::Error;
                       this->m_data.validationMessage = "Nothing writes to this attachments";
                   }
               }
            }
        }

    };

    class NodeRenderPass : public FGQBase
    {
        Q_OBJECT
    public:
        NodeRenderPass()
        {
              //m_data.outNodes = { RawImageOut };
              m_data.name = RENDERPASS_NAME_STR;
              m_data.caption = "Render Pass";
              m_data.inNodes  = {
                                    {SampledImageID, "Sampled Image"},
                                    {SampledImageID, "Sampled Image"},
                                    {SampledImageID, "Sampled Image"},
                                    {SampledImageID, "Sampled Image"},
                                    {SampledImageID, "Sampled Image"},
                                    {SampledImageID, "Sampled Image"},
                                    {SampledImageID, "Sampled Image"},
                                    {SampledImageID, "Sampled Image"}
                                };
              m_data.outNodes = { {RenderPassColorAttachmentID, "Color Attachment"},
                                  {RenderPassColorAttachmentID, "Color Attachment"},
                                  {RenderPassColorAttachmentID, "Color Attachment"},
                                  {RenderPassColorAttachmentID, "Color Attachment"},
                                  {RenderPassColorAttachmentID, "Color Attachment"},
                                  {RenderPassColorAttachmentID, "Color Attachment"},
                                  {RenderPassColorAttachmentID, "Color Attachment"},
                                  {RenderPassColorAttachmentID, "Color Attachment"},
                                  {RenderPassDepthAttachmentID, "DepthStencil Attachment"}
                                };
              m_data.m_widget = new QJForm::QJForm();
              m_data.m_widget->setSchema(
                          ObjectFromString(
                              R"foo(
{
    "type" : "object",
    "properties" : {
        "name" : {
            "type" : "string"
        }
    },
    "ui:order" : ["name"]
}
                              )foo"
                          ));
              m_data.m_widget->setStyleSheet("background-color: rgba(0,0,0,0)");

              QtNodes::NodeStyle style(
              R"(
              {
              "NodeStyle": {
                "NormalBoundaryColor": [0, 255, 0],
                "SelectedBoundaryColor": [255, 165, 0],
                "GradientColor0": "grey",
                "GradientColor1": [80, 80, 80],
                "GradientColor2": [64, 64, 64],
                "GradientColor3": [58, 58, 58],
                "ShadowColor"   : [255, 255, 255],
                "FontColor" : "white",
                "FontColorFaded" : "gray",
                "ConnectionPointColor": [169, 169, 169],
                "FilledConnectionPointColor": "cyan",
                "ErrorColor": "red",
                "WarningColor": [128, 128, 0],

                "PenWidth": 2.0,
                "HoveredPenWidth": 4.5,

                "ConnectionPointDiameter": 8.0,

                "Opacity": 1.0
              }
              }
              )");
              setNodeStyle( style );
        }
        virtual ~NodeRenderPass()
        {
        }
    };

    class NodeSwapchainPass : public FGQBase
    {
        Q_OBJECT
    public:
        NodeSwapchainPass()
        {
              //m_data.outNodes = { RawImageOut };
              m_data.name = SWAPCHAIN_PASS_NAME_STR;
              m_data.caption = "Swapchain Render Pass";
              m_data.inNodes  = {
                                      {SampledImageID, "Sampled Image"},
                                      {SampledImageID, "Sampled Image"},
                                      {SampledImageID, "Sampled Image"},
                                      {SampledImageID, "Sampled Image"},
                                      {SampledImageID, "Sampled Image"},
                                      {SampledImageID, "Sampled Image"},
                                      {SampledImageID, "Sampled Image"},
                                      {SampledImageID, "Sampled Image"},
                                    {SampledImageID, "Sampled Image"}
                                };

              //m_data.outNodes = { {RenderPassDepthAttachmentID, "Depth Attachment"} };
              m_data.m_widget = new QJForm::QJForm();
              m_data.m_widget->setSchema(
                          ObjectFromString(
                              R"foo(
{
    "type" : "object",
    "properties" : {
        "name" : {
            "type" : "string"
        }
    },
    "ui:order" : ["name"]
}
                              )foo"
                          ));
              m_data.m_widget->setStyleSheet("background-color: rgba(0,0,0,0)");
              QtNodes::NodeStyle style(
              R"(
              {
              "NodeStyle": {
                "NormalBoundaryColor": [0, 255, 0],
                "SelectedBoundaryColor": [255, 165, 0],
                "GradientColor0": "grey",
                "GradientColor1": [80, 80, 80],
                "GradientColor2": [64, 64, 64],
                "GradientColor3": [58, 58, 58],
                "ShadowColor"   : [255, 255, 255],
                "FontColor" : "white",
                "FontColorFaded" : "gray",
                "ConnectionPointColor": [169, 169, 169],
                "FilledConnectionPointColor": "cyan",
                "ErrorColor": "red",
                "WarningColor": [128, 128, 0],

                "PenWidth": 2.0,
                "HoveredPenWidth": 4.5,

                "ConnectionPointDiameter": 8.0,

                "Opacity": 1.0
              }
              }
              )");
              setNodeStyle( style );
        }
        virtual ~NodeSwapchainPass()
        {
        }
    };

}
