#include <nodes/NodeData>
#include <nodes/FlowScene>
#include <nodes/FlowView>

#include <QtWidgets/QApplication>
#include <QtWidgets/QApplication>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QMenuBar>

#include <nodes/DataModelRegistry>

//#include "TextSourceDataModel.hpp"
#include "TextDisplayDataModel.hpp"

using QtNodes::DataModelRegistry;
using QtNodes::FlowView;
using QtNodes::FlowScene;

static std::shared_ptr<DataModelRegistry>
registerDataModels()
{
  auto ret = std::make_shared<DataModelRegistry>();

  //ret->registerModel<TextSourceDataModel>();
  //ret->registerModel<TextDisplayDataModel>();
  //ret->registerModel<RenderPassDataModel>();
  //ret->registerModel<ImageDataModel>();
  //ret->registerModel<FGQ::FGQBase>();
  //ret->registerModel<FGQ::NodeSwapchainImage>("Physical Data");
  ret->registerModel<FGQ::NodeRenderPass>("1.Render Passes");
  ret->registerModel<FGQ::NodeSwapchainPass>("1.Render Passes");

  ret->registerModel<FGQ::NodeColorAttachment>("2.RenderPass Outputs");
  ret->registerModel<FGQ::NodeDepthStencilAttachment>("2.RenderPass Outputs");

  ret->registerModel<FGQ::NodeInputSampledImage>("3.RenderPass Inputs");

  ret->registerModel<FGQ::NodePhysicalColorImage>("4.Images");
  ret->registerModel<FGQ::NodeDepthStencilImage>( "4.Images");
  //ret->registerModel<FGQ::NodePhysicalDepthImage>("Physical Data");
  //ret->registerModel<FGQ::NodeColorAttachment>("Attachments");
  //ret->registerModel<FGQ::NodeDepthStencilAttachment>("Attachments");
  //ret->registerModel<FGQ::NodeDepthAttachment>("Attachments");
  //ret->registerModel<FGQ::NodeInputTexture>("Textures");
  //ret->registerModel<RenderTargetDataModel>();

  return ret;
}

int
main(int argc, char *argv[])
{
  QApplication app(argc, argv);

//  setStyle();

  QWidget mainWidget;

  auto menuBar    = new QMenuBar();
  auto saveAction = menuBar->addAction("Save..");
  auto loadAction = menuBar->addAction("Load..");

  QVBoxLayout *l = new QVBoxLayout(&mainWidget);

  l->addWidget(menuBar);
  auto scene = new FlowScene(registerDataModels(), &mainWidget);
  l->addWidget(new FlowView(scene));
  l->setContentsMargins(0, 0, 0, 0);
  l->setSpacing(0);

  QObject::connect(saveAction, &QAction::triggered,
                   scene, &FlowScene::save);

  QObject::connect(loadAction, &QAction::triggered,
                   scene, &FlowScene::load);

  mainWidget.setWindowTitle("Dataflow tools: simplest calculator");
  mainWidget.resize(800, 600);
  mainWidget.showNormal();

  return app.exec();
}
