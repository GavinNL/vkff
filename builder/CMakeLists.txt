cmake_minimum_required(VERSION 3.10)


################################################################################
# This Top-level project is for a single Top-level library
#
# A single library
# Unit tests for the library
# A set of executables which use this library
################################################################################
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(Qt5 COMPONENTS Core Widgets REQUIRED)
################################################################################



################################################################################
add_subdirectory(third_party/QJForm)
add_subdirectory(third_party/nodeeditor)
################################################################################



################################################################################
# Build the Library.
#  By default, the library name will be the ${PROJECT_NAME}
#
#  If you wish to add more libraries, make a copy of this section and
# change the values below.
################################################################################
set(outName              framegraph-builder)   # name of the library
#-------------------------------------------------------------------------------

file(GLOB_RECURSE srcFiles "src/*" )
add_executable( ${outName} ${srcFiles} )

target_include_directories( ${outName}
                            PUBLIC
                               "$<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}/include>"
                               "$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>"
)

target_compile_features( ${outName}
                          PUBLIC
                              cxx_std_17)

target_compile_definitions( ${outName}
                                PUBLIC
                                TEST_DEFINE)

target_link_libraries( ${outName}  PRIVATE NodeEditor::nodes QJForm::QJForm )


include(GNUInstallDirs)
install(
   TARGETS
       ${outName}
   LIBRARY  DESTINATION "${CMAKE_INSTALL_LIBDIR}"
   ARCHIVE  DESTINATION "${CMAKE_INSTALL_LIBDIR}"
   RUNTIME  DESTINATION "${CMAKE_INSTALL_BINDIR}"
   INCLUDES DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}"
  )
################################################################################



#####################################################
## PACKAGE
#####################################################
SET(CPACK_PACKAGE_NAME          ${PROJECT_NAME})
SET(CPACK_PACKAGE_VENDOR        ${PROJECT_NAME})
SET(CPACK_PACKAGE_EXECUTABLES   ${PROJECT_NAME};${PROJECT_NAME})

SET(CPACK_PACKAGE_INSTALL_REGISTRY_KEY "${PROJECT_NAME}")
SET(CPACK_PACKAGE_VERSION       "${PROJECT_VERSION}")
SET(CPACK_PACKAGE_VERSION_MAJOR "${PROJECT_VERSION_MAJOR}")
SET(CPACK_PACKAGE_VERSION_MINOR "${PROJECT_VERSION_MINOR}")
SET(CPACK_PACKAGE_VERSION_PATCH "${PROJECT_VERSION_PATCH}")
SET(CPACK_PACKAGE_CONTACT       "me")

INCLUDE(InstallRequiredSystemLibraries)

IF(WIN32 AND NOT UNIX)
  SET(CPACK_NSIS_INSTALLED_ICON_NAME   "bin\\\\${PROJECT_NAME}.exe")
  SET(CPACK_NSIS_DISPLAY_NAME          "${CPACK_PACKAGE_INSTALL_DIRECTORY}${PROJECT_NAME}")
  SET(CPACK_NSIS_MODIFY_PATH           ON)
ENDIF(WIN32 AND NOT UNIX)

INCLUDE(CPack)


return()
