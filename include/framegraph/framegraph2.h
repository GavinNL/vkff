#ifndef VKA_FRAMEGRAPH_2_H
#define  VKA_FRAMEGRAPH_2_H

#include <vulkan/vulkan.hpp>
#include <nlohmann/json.hpp>
#include <iostream>
#include <set>
#include "defines.h"

namespace vka
{

struct FrameGraphDispatcher
{
    vk::Device         device;
    vk::PhysicalDevice physicalDevice;
    vk::Queue          graphicsQueue;
    vk::SwapchainKHR   swapchain;

    vk::Image create(vk::ImageCreateInfo const & I)
    {
        auto i = device.createImage(I);
        return i;
    }
    void destroy(vk::Image I)
    {
        device.destroyImage(I);
    }

    vk::DeviceMemory allocateMemoryForImage(vk::Image img, vk::MemoryPropertyFlags properties)
    {
        vk::MemoryAllocateInfo  memInfo;
        vk::MemoryRequirements  memRequirements = device.getImageMemoryRequirements(img);

        memInfo.allocationSize  = memRequirements.size;
        memInfo.memoryTypeIndex = findMemoryType( physicalDevice,
                                                  memRequirements.memoryTypeBits,
                                                  properties);

       auto memory = device.allocateMemory(memInfo);
       return memory;
    }
    void destroy(vk::DeviceMemory m)
    {
        device.freeMemory(m);
    }


    static uint32_t findMemoryType(vk::PhysicalDevice physicalDevice, uint32_t typeFilter, vk::MemoryPropertyFlags properties)
    {
        auto memProperites        = physicalDevice.getMemoryProperties();

        for (uint32_t i = 0; i < memProperites.memoryTypeCount ; i++)
        {
            const vk::MemoryPropertyFlags MemPropFlags = static_cast<vk::MemoryPropertyFlags>(memProperites.memoryTypes[i].propertyFlags);
            #define GET_BIT(a, j) (a & (1u << j))

            if ( GET_BIT(typeFilter,i) && ( MemPropFlags & properties) == properties)
            {
                return i;
            }
        }
        throw std::runtime_error("failed to find suitable memory type!");
    }
};


/**
 * @brief The FrameGraphExecutor struct
 *
 * This is a virtual class which you derive your executor from.
 * The executor is responsible for writing the commands to the command
 * buffer.
 */
struct FrameGraphExecutor
{


    struct RenderPassInputAttachment
    {
        std::string   name;
        vk::Format    format;
        vk::Extent2D  extent;
        vk::Image     image;
        vk::ImageView view;
        vk::Sampler   sampler;
    };

    struct RenderPassDestroy
    {
        std::string                              renderPassName; // the name of the render pass that needs to be initialized
    };

    struct RenderPassInit
    {
        std::string renderPassName; // the name of the renderpass we are are writing for
        vk::RenderPass renderPass;
        // a vector of input textures which can be used to
        // for the renderpass. You can use this data to
        // create descriptor sets for your Pipelines.
        std::vector< RenderPassInputAttachment > inputSampledImages;


        // A list of output textures which you will be writing to
        //std::vector< RenderPassInputAttachment > outputAttachments;


        // the renderpass object that you can use for
        // all the pipelines in this pass.
    };

    struct RenderPassWrite : public RenderPassInit
    {
        vk::CommandBuffer                        cmd;
        vk::RenderPassBeginInfo                  renderPassBeginInfo;
        std::vector< vk::ClearValue >            clearValues;
        uint32_t                                 frameIndex;
        vk::RenderPass                           renderPass;
        vk::Framebuffer                          frameBuffer;
        vk::Extent2D                             frameBufferExtent;
    };

    virtual ~FrameGraphExecutor(){}

    /**
     * @brief init
     *
     * Called when the frame graph has been initialized.
     * Use this to allocate any resources that this render pass will use.
     * The RenderPassInit struct contains information about the input
     * and output attachments
     */
    virtual void init(RenderPassInit & I) = 0;

    /**
     * @brief destroy
     *
     * Called when the framegraph is destroyed.
     * You can use this to destroy any descriptor sets which are dependent
     * on input input attachmet sizes. For example, if the size of the window
     * changes, and new gBuffer images has to be reallocated. You will then
     * need to create new descriptor sets for the new images, since they
     * will be invalidated.
     */
    virtual void destroy(RenderPassDestroy & D) = 0;


    /**
     * @brief write
     *
     * Called when you need to write the command buffers for
     * this particular renderpass.
     */
    virtual void write(RenderPassWrite & C) = 0;
};

struct ImageData
{
    //=====================
    // User provided
    //=====================
    std::string name;
    vk::Format  format;

    ImageData() : name(""), format(vk::Format::eR8G8B8A8Unorm)
    {
    }
    ImageData(std::string _name, vk::Format f) : name(_name), format(f){}

protected:
    //=====================
    // calculated
    //=====================
    vk::ImageUsageFlags _usage;// = vk::ImageUsageFlagBits::eSampled | vk::ImageUsageFlagBits::eTransferSrc;
    vk::Extent2D        _extent;
    size_t              _renderPassIndex; // which render pass writes to this image
    bool                _dependsOnSwapchain=true; // this image is the same size as the swapchain.

    //=====================
    // created
    //=====================
    vk::Image        _image;
    vk::ImageView    _imageView;
    vk::DeviceMemory _imageMemory;

    friend struct FrameGraph2;
};

inline void from_json(nlohmann::json const & J, ImageData & I )
{
    auto formatstr = J.at("format").get<std::string>();
    if( J.count("name"))
        I.name = J.at("name").get<std::string>();

#define PARSEFORMAT(A) if( formatstr == # A) {I.format = vk::Format::e ## A; return; }
    PARSEFORMAT(R4G4UnormPack8)
    PARSEFORMAT(R4G4B4A4UnormPack16)
    PARSEFORMAT(B4G4R4A4UnormPack16)
    PARSEFORMAT(R5G6B5UnormPack16)
    PARSEFORMAT(B5G6R5UnormPack16)
    PARSEFORMAT(R5G5B5A1UnormPack16)
    PARSEFORMAT(B5G5R5A1UnormPack16)
    PARSEFORMAT(A1R5G5B5UnormPack16)
    PARSEFORMAT(R8Unorm)
    PARSEFORMAT(R8Snorm)
    PARSEFORMAT(R8Uscaled)
    PARSEFORMAT(R8Sscaled)
    PARSEFORMAT(R8Uint)
    PARSEFORMAT(R8Sint)
    PARSEFORMAT(R8Srgb)
    PARSEFORMAT(R8G8Unorm)
    PARSEFORMAT(R8G8Snorm)
    PARSEFORMAT(R8G8Uscaled)
    PARSEFORMAT(R8G8Sscaled)
    PARSEFORMAT(R8G8Uint)
    PARSEFORMAT(R8G8Sint)
    PARSEFORMAT(R8G8Srgb)
    PARSEFORMAT(R8G8B8Unorm)
    PARSEFORMAT(R8G8B8Snorm)
    PARSEFORMAT(R8G8B8Uscaled)
    PARSEFORMAT(R8G8B8Sscaled)
    PARSEFORMAT(R8G8B8Uint)
    PARSEFORMAT(R8G8B8Sint)
    PARSEFORMAT(R8G8B8Srgb)
    PARSEFORMAT(B8G8R8Unorm)
    PARSEFORMAT(B8G8R8Snorm)
    PARSEFORMAT(B8G8R8Uscaled)
    PARSEFORMAT(B8G8R8Sscaled)
    PARSEFORMAT(B8G8R8Uint)
    PARSEFORMAT(B8G8R8Sint)
    PARSEFORMAT(B8G8R8Srgb)
    PARSEFORMAT(R8G8B8A8Unorm)
    PARSEFORMAT(R8G8B8A8Snorm)
    PARSEFORMAT(R8G8B8A8Uscaled)
    PARSEFORMAT(R8G8B8A8Sscaled)
    PARSEFORMAT(R8G8B8A8Uint)
    PARSEFORMAT(R8G8B8A8Sint)
    PARSEFORMAT(R8G8B8A8Srgb)
    PARSEFORMAT(B8G8R8A8Unorm)
    PARSEFORMAT(B8G8R8A8Snorm)
    PARSEFORMAT(B8G8R8A8Uscaled)
    PARSEFORMAT(B8G8R8A8Sscaled)
    PARSEFORMAT(B8G8R8A8Uint)
    PARSEFORMAT(B8G8R8A8Sint)
    PARSEFORMAT(B8G8R8A8Srgb)
    PARSEFORMAT(A8B8G8R8UnormPack32)
    PARSEFORMAT(A8B8G8R8SnormPack32)
    PARSEFORMAT(A8B8G8R8UscaledPack32)
    PARSEFORMAT(A8B8G8R8SscaledPack32)
    PARSEFORMAT(A8B8G8R8UintPack32)
    PARSEFORMAT(A8B8G8R8SintPack32)
    PARSEFORMAT(A8B8G8R8SrgbPack32)
    PARSEFORMAT(A2R10G10B10UnormPack32)
    PARSEFORMAT(A2R10G10B10SnormPack32)
    PARSEFORMAT(A2R10G10B10UscaledPack32)
    PARSEFORMAT(A2R10G10B10SscaledPack32)
    PARSEFORMAT(A2R10G10B10UintPack32)
    PARSEFORMAT(A2R10G10B10SintPack32)
    PARSEFORMAT(A2B10G10R10UnormPack32)
    PARSEFORMAT(A2B10G10R10SnormPack32)
    PARSEFORMAT(A2B10G10R10UscaledPack32)
    PARSEFORMAT(A2B10G10R10SscaledPack32)
    PARSEFORMAT(A2B10G10R10UintPack32)
    PARSEFORMAT(A2B10G10R10SintPack32)
    PARSEFORMAT(R16Unorm)
    PARSEFORMAT(R16Snorm)
    PARSEFORMAT(R16Uscaled)
    PARSEFORMAT(R16Sscaled)
    PARSEFORMAT(R16Uint)
    PARSEFORMAT(R16Sint)
    PARSEFORMAT(R16Sfloat)
    PARSEFORMAT(R16G16Unorm)
    PARSEFORMAT(R16G16Snorm)
    PARSEFORMAT(R16G16Uscaled)
    PARSEFORMAT(R16G16Sscaled)
    PARSEFORMAT(R16G16Uint)
    PARSEFORMAT(R16G16Sint)
    PARSEFORMAT(R16G16Sfloat)
    PARSEFORMAT(R16G16B16Unorm)
    PARSEFORMAT(R16G16B16Snorm)
    PARSEFORMAT(R16G16B16Uscaled)
    PARSEFORMAT(R16G16B16Sscaled)
    PARSEFORMAT(R16G16B16Uint)
    PARSEFORMAT(R16G16B16Sint)
    PARSEFORMAT(R16G16B16Sfloat)
    PARSEFORMAT(R16G16B16A16Unorm)
    PARSEFORMAT(R16G16B16A16Snorm)
    PARSEFORMAT(R16G16B16A16Uscaled)
    PARSEFORMAT(R16G16B16A16Sscaled)
    PARSEFORMAT(R16G16B16A16Uint)
    PARSEFORMAT(R16G16B16A16Sint)
    PARSEFORMAT(R16G16B16A16Sfloat)
    PARSEFORMAT(R32Uint)
    PARSEFORMAT(R32Sint)
    PARSEFORMAT(R32Sfloat)
    PARSEFORMAT(R32G32Uint)
    PARSEFORMAT(R32G32Sint)
    PARSEFORMAT(R32G32Sfloat)
    PARSEFORMAT(R32G32B32Uint)
    PARSEFORMAT(R32G32B32Sint)
    PARSEFORMAT(R32G32B32Sfloat)
    PARSEFORMAT(R32G32B32A32Uint)
    PARSEFORMAT(R32G32B32A32Sint)
    PARSEFORMAT(R32G32B32A32Sfloat)
    PARSEFORMAT(R64Uint)
    PARSEFORMAT(R64Sint)
    PARSEFORMAT(R64Sfloat)
    PARSEFORMAT(R64G64Uint)
    PARSEFORMAT(R64G64Sint)
    PARSEFORMAT(R64G64Sfloat)
    PARSEFORMAT(R64G64B64Uint)
    PARSEFORMAT(R64G64B64Sint)
    PARSEFORMAT(R64G64B64Sfloat)
    PARSEFORMAT(R64G64B64A64Uint)
    PARSEFORMAT(R64G64B64A64Sint)
    PARSEFORMAT(R64G64B64A64Sfloat)
    PARSEFORMAT(B10G11R11UfloatPack32)
    PARSEFORMAT(E5B9G9R9UfloatPack32)
    PARSEFORMAT(D16Unorm)
    PARSEFORMAT(X8D24UnormPack32)
    PARSEFORMAT(D32Sfloat)
    PARSEFORMAT(S8Uint)
    PARSEFORMAT(D16UnormS8Uint)
    PARSEFORMAT(D24UnormS8Uint)
    PARSEFORMAT(D32SfloatS8Uint)
    PARSEFORMAT(Bc1RgbUnormBlock)
    PARSEFORMAT(Bc1RgbSrgbBlock)
    PARSEFORMAT(Bc1RgbaUnormBlock)
    PARSEFORMAT(Bc1RgbaSrgbBlock)
    PARSEFORMAT(Bc2UnormBlock)
    PARSEFORMAT(Bc2SrgbBlock)
    PARSEFORMAT(Bc3UnormBlock)
    PARSEFORMAT(Bc3SrgbBlock)
    PARSEFORMAT(Bc4UnormBlock)
    PARSEFORMAT(Bc4SnormBlock)
    PARSEFORMAT(Bc5UnormBlock)
    PARSEFORMAT(Bc5SnormBlock)
    PARSEFORMAT(Bc6HUfloatBlock)
    PARSEFORMAT(Bc6HSfloatBlock)
    PARSEFORMAT(Bc7UnormBlock)
    PARSEFORMAT(Bc7SrgbBlock)
    PARSEFORMAT(Etc2R8G8B8UnormBlock)
    PARSEFORMAT(Etc2R8G8B8SrgbBlock)
    PARSEFORMAT(Etc2R8G8B8A1UnormBlock)
    PARSEFORMAT(Etc2R8G8B8A1SrgbBlock)
    PARSEFORMAT(Etc2R8G8B8A8UnormBlock)
    PARSEFORMAT(Etc2R8G8B8A8SrgbBlock)
    PARSEFORMAT(EacR11UnormBlock)
    PARSEFORMAT(EacR11SnormBlock)
    PARSEFORMAT(EacR11G11UnormBlock)
    PARSEFORMAT(EacR11G11SnormBlock)
    PARSEFORMAT(Astc4x4UnormBlock)
    PARSEFORMAT(Astc4x4SrgbBlock)
    PARSEFORMAT(Astc5x4UnormBlock)
    PARSEFORMAT(Astc5x4SrgbBlock)
    PARSEFORMAT(Astc5x5UnormBlock)
    PARSEFORMAT(Astc5x5SrgbBlock)
    PARSEFORMAT(Astc6x5UnormBlock)
    PARSEFORMAT(Astc6x5SrgbBlock)
    PARSEFORMAT(Astc6x6UnormBlock)
    PARSEFORMAT(Astc6x6SrgbBlock)
    PARSEFORMAT(Astc8x5UnormBlock)
    PARSEFORMAT(Astc8x5SrgbBlock)
    PARSEFORMAT(Astc8x6UnormBlock)
    PARSEFORMAT(Astc8x6SrgbBlock)
    PARSEFORMAT(Astc8x8UnormBlock)
    PARSEFORMAT(Astc8x8SrgbBlock)
    PARSEFORMAT(Astc10x5UnormBlock)
    PARSEFORMAT(Astc10x5SrgbBlock)
    PARSEFORMAT(Astc10x6UnormBlock)
    PARSEFORMAT(Astc10x6SrgbBlock)
    PARSEFORMAT(Astc10x8UnormBlock)
    PARSEFORMAT(Astc10x8SrgbBlock)
    PARSEFORMAT(Astc10x10UnormBlock)
    PARSEFORMAT(Astc10x10SrgbBlock)
    PARSEFORMAT(Astc12x10UnormBlock)
    PARSEFORMAT(Astc12x10SrgbBlock)
    PARSEFORMAT(Astc12x12UnormBlock)
    PARSEFORMAT(Astc12x12SrgbBlock)
    PARSEFORMAT(G8B8G8R8422Unorm)
    PARSEFORMAT(B8G8R8G8422Unorm)
    PARSEFORMAT(G8B8R83Plane420Unorm)
    PARSEFORMAT(G8B8R82Plane420Unorm)
    PARSEFORMAT(G8B8R83Plane422Unorm)
    PARSEFORMAT(G8B8R82Plane422Unorm)
    PARSEFORMAT(G8B8R83Plane444Unorm)
    PARSEFORMAT(R10X6UnormPack16)
    PARSEFORMAT(R10X6G10X6Unorm2Pack16)
    PARSEFORMAT(R10X6G10X6B10X6A10X6Unorm4Pack16)
    PARSEFORMAT(G10X6B10X6G10X6R10X6422Unorm4Pack16)
    PARSEFORMAT(B10X6G10X6R10X6G10X6422Unorm4Pack16)
    PARSEFORMAT(G10X6B10X6R10X63Plane420Unorm3Pack16)
    PARSEFORMAT(G10X6B10X6R10X62Plane420Unorm3Pack16)
    PARSEFORMAT(G10X6B10X6R10X63Plane422Unorm3Pack16)
    PARSEFORMAT(G10X6B10X6R10X62Plane422Unorm3Pack16)
    PARSEFORMAT(G10X6B10X6R10X63Plane444Unorm3Pack16)
    PARSEFORMAT(R12X4UnormPack16)
    PARSEFORMAT(R12X4G12X4Unorm2Pack16)
    PARSEFORMAT(R12X4G12X4B12X4A12X4Unorm4Pack16)
    PARSEFORMAT(G12X4B12X4G12X4R12X4422Unorm4Pack16)
    PARSEFORMAT(B12X4G12X4R12X4G12X4422Unorm4Pack16)
    PARSEFORMAT(G12X4B12X4R12X43Plane420Unorm3Pack16)
    PARSEFORMAT(G12X4B12X4R12X42Plane420Unorm3Pack16)
    PARSEFORMAT(G12X4B12X4R12X43Plane422Unorm3Pack16)
    PARSEFORMAT(G12X4B12X4R12X42Plane422Unorm3Pack16)
    PARSEFORMAT(G12X4B12X4R12X43Plane444Unorm3Pack16)
    PARSEFORMAT(G16B16G16R16422Unorm)
    PARSEFORMAT(B16G16R16G16422Unorm)
    PARSEFORMAT(G16B16R163Plane420Unorm)
    PARSEFORMAT(G16B16R162Plane420Unorm)
    PARSEFORMAT(G16B16R163Plane422Unorm)
    PARSEFORMAT(G16B16R162Plane422Unorm)
    PARSEFORMAT(G16B16R163Plane444Unorm)
    PARSEFORMAT(Pvrtc12BppUnormBlockIMG)
    PARSEFORMAT(Pvrtc14BppUnormBlockIMG)
    PARSEFORMAT(Pvrtc22BppUnormBlockIMG)
    PARSEFORMAT(Pvrtc24BppUnormBlockIMG)
    PARSEFORMAT(Pvrtc12BppSrgbBlockIMG)
    PARSEFORMAT(Pvrtc14BppSrgbBlockIMG)
    PARSEFORMAT(Pvrtc22BppSrgbBlockIMG)
    PARSEFORMAT(Pvrtc24BppSrgbBlockIMG)
    PARSEFORMAT(Astc4x4SfloatBlockEXT)
    PARSEFORMAT(Astc5x4SfloatBlockEXT)
    PARSEFORMAT(Astc5x5SfloatBlockEXT)
    PARSEFORMAT(Astc6x5SfloatBlockEXT)
    PARSEFORMAT(Astc6x6SfloatBlockEXT)
    PARSEFORMAT(Astc8x5SfloatBlockEXT)
    PARSEFORMAT(Astc8x6SfloatBlockEXT)
    PARSEFORMAT(Astc8x8SfloatBlockEXT)
    PARSEFORMAT(Astc10x5SfloatBlockEXT)
    PARSEFORMAT(Astc10x6SfloatBlockEXT)
    PARSEFORMAT(Astc10x8SfloatBlockEXT)
    PARSEFORMAT(Astc10x10SfloatBlockEXT)
    PARSEFORMAT(Astc12x10SfloatBlockEXT)
    PARSEFORMAT(Astc12x12SfloatBlockEXT)
    PARSEFORMAT(G8B8G8R8422UnormKHR)
    PARSEFORMAT(B8G8R8G8422UnormKHR)
    PARSEFORMAT(G8B8R83Plane420UnormKHR)
    PARSEFORMAT(G8B8R82Plane420UnormKHR)
    PARSEFORMAT(G8B8R83Plane422UnormKHR)
    PARSEFORMAT(G8B8R82Plane422UnormKHR)
    PARSEFORMAT(G8B8R83Plane444UnormKHR)
    PARSEFORMAT(R10X6UnormPack16KHR)
    PARSEFORMAT(R10X6G10X6Unorm2Pack16KHR)
    PARSEFORMAT(R10X6G10X6B10X6A10X6Unorm4Pack16KHR)
    PARSEFORMAT(G10X6B10X6G10X6R10X6422Unorm4Pack16KHR)
    PARSEFORMAT(B10X6G10X6R10X6G10X6422Unorm4Pack16KHR)
    PARSEFORMAT(G10X6B10X6R10X63Plane420Unorm3Pack16KHR)
    PARSEFORMAT(G10X6B10X6R10X62Plane420Unorm3Pack16KHR)
    PARSEFORMAT(G10X6B10X6R10X63Plane422Unorm3Pack16KHR)
    PARSEFORMAT(G10X6B10X6R10X62Plane422Unorm3Pack16KHR)
    PARSEFORMAT(G10X6B10X6R10X63Plane444Unorm3Pack16KHR)
    PARSEFORMAT(R12X4UnormPack16KHR)
    PARSEFORMAT(R12X4G12X4Unorm2Pack16KHR)
    PARSEFORMAT(R12X4G12X4B12X4A12X4Unorm4Pack16KHR)
    PARSEFORMAT(G12X4B12X4G12X4R12X4422Unorm4Pack16KHR)
    PARSEFORMAT(B12X4G12X4R12X4G12X4422Unorm4Pack16KHR)
    PARSEFORMAT(G12X4B12X4R12X43Plane420Unorm3Pack16KHR)
    PARSEFORMAT(G12X4B12X4R12X42Plane420Unorm3Pack16KHR)
    PARSEFORMAT(G12X4B12X4R12X43Plane422Unorm3Pack16KHR)
    PARSEFORMAT(G12X4B12X4R12X42Plane422Unorm3Pack16KHR)
    PARSEFORMAT(G12X4B12X4R12X43Plane444Unorm3Pack16KHR)
    PARSEFORMAT(G16B16G16R16422UnormKHR)
    PARSEFORMAT(B16G16R16G16422UnormKHR)
    PARSEFORMAT(G16B16R163Plane420UnormKHR)
    PARSEFORMAT(G16B16R162Plane420UnormKHR)
    PARSEFORMAT(G16B16R163Plane422UnormKHR)
    PARSEFORMAT(G16B16R162Plane422UnormKHR)
    PARSEFORMAT(G16B16R163Plane444UnormKHR)
    #undef PARSEFORMAT
}

struct RenderPassData
{
    //=====================
    // User provided
    //=====================
    std::string name;
    std::vector<size_t>         colorAttachmentIndex; // index into the .images array to be used as colour outputs.
    std::optional<size_t>       depthAttachmentIndex; // the index to use for depth

    std::vector<size_t>         inputSamplers; // index into the .sampledImages array to be used as colour outputs.
    std::vector<vk::ClearValue> clearValues;

    RenderPassData()
    {
    }

    RenderPassData( std::string _name,
                    std::vector<size_t> _inputSampledImages,
                    std::vector<size_t> _outputAttachments,
                    std::optional<size_t> _depthOutput) : name(_name), colorAttachmentIndex(_outputAttachments), depthAttachmentIndex(_depthOutput), inputSamplers(_inputSampledImages)
    {
    }

protected:
    bool _toDestroy=false;
    bool _toInit=true;
    bool _initialized=false;

    //=====================
    // created
    //=====================
    vk::RenderPass        _renderPass;
    vk::Framebuffer       _frameBuffer;

    friend struct FrameGraph2;
};

inline void from_json(nlohmann::json const & J, RenderPassData & I)
{
    if( J.count("colorAttachments") )
        I.colorAttachmentIndex = J.at("colorAttachments").get< std::vector<size_t> >();

    if( J.count("depthAttachment") )
        I.depthAttachmentIndex = J.at("depthAttachment").get< size_t >();

    I.name = J.at("name").get<std::string>();
}

struct ColorAttachment
{
    std::string          name;
    size_t               imageIndex = std::numeric_limits<size_t>::max();
    //vk::ClearColorValue  clearValue = std::array<float,4>{{0.f,0.f,0.f,1.0f}};
    vk::ClearValue       clearValue = vk::ClearColorValue( std::array<float,4>{{0.f,0.f,0.f,1.0f}} );

    ColorAttachment()
    {
    }

    ColorAttachment(size_t _imageIndex) : imageIndex(_imageIndex)
    {
    }

protected:
    //
    size_t          _renderPassWriter = std::numeric_limits<size_t>::max();
    vk::ImageLayout _finalLayout      = vk::ImageLayout::ePresentSrcKHR;
    friend struct FrameGraph2;
};

void from_json(nlohmann::json const &J, ColorAttachment & I)
{
    if( J.count("name"))
        I.name = J.at("name").get<std::string>();
    I.imageIndex = J.at("imageIndex").get<size_t>();

    if( J.count("clearValue"))
    {
        auto clear = J.at("clearValue").get< std::vector<float> >();
    }

}

struct SampledImage
{
    size_t attachmentIndex; // which ColorAttachment should we take this from

    SampledImage()
    {
    }

    SampledImage(size_t _i) : attachmentIndex(_i)
    {}

protected:
    vk::Sampler _sampler;
    friend struct FrameGraph2;
};

void from_json(nlohmann::json const &J, SampledImage & I)
{
    I.attachmentIndex = J.at("attachmentIndex").get<size_t>();
}

struct SwaphainData
{
    std::string name;
    std::vector<size_t>          inputSamplers;// = std::vector<size_t>(25, 0xFFFFFFFFFFFFFFFF); // index into the .sampledImages

protected:
    bool                         _toInit=true;
    bool _toDestroy=false;
    bool _initialized=false;
    friend struct FrameGraph2;
};

inline void from_json(nlohmann::json const & J, SwaphainData & I)
{
    if( J.count("samplers") )
        I.inputSamplers = J.at("samplers").get< std::vector<size_t> >();
    I.name = J.at("name").get<std::string>();
}

struct FrameGraph2
{


    std::vector<ImageData>       images;
    std::vector<ColorAttachment> attachments;
    std::vector<SampledImage>    samplers;
    std::vector<RenderPassData>  renderPasses;
    SwaphainData                 swapchain;

    struct ResizeInfo
    {
        // Required values
        uint32_t                   width;
        uint32_t                   height;
    };

    /**
     * @brief resize
     * @param width
     * @param height
     *
     * If your swapchain changes size, call this method
     * before doing any framegraph related things. This
     * will reallocate any internal images that are required
     */
    void resize(ResizeInfo const & info)
    {
        // destroy the swapchain.
        uint32_t width = info.width;
        uint32_t height = info.height;

        for(auto & I : renderPasses)
        {
            if( I._initialized)
                I._toDestroy = true;
        }
        if( swapchain._initialized)
        {
            swapchain._toDestroy = true;
            swapchain._toInit= true;
        }

        for(auto & I : images)
        {
            if( I._dependsOnSwapchain )
            {
                I._extent.width  = width;
                I._extent.height = height;
            }
        }
        for(auto & I : images)
        {
            // if the image was already created and it depends on the swapchain,
            // destroy it. and then reinitialize it
            if( I._image && I._dependsOnSwapchain ) _freeImage(I);

            _createImage(I);
        }

        for(auto & I : samplers)
        {
            if( !I._sampler)
                _createSampledImage(I);
        }

        for(auto & r : renderPasses)
        {
            _destroyRenderPass(r);
         //   _createRenderPass(r);
        }
    }

    void free()
    {
        for(auto & I : images)
        {
            _freeImage(I);
        }

        for(auto & I : samplers)
        {
            if (I._sampler)
            {
                m_dispatcher.device.destroySampler(I._sampler);
                I._sampler = vk::Sampler();
            }
        }
        for(auto & r : renderPasses)
        {
            _destroyRenderPass(r);
        }
    }

    void _freeImage(ImageData & I)
    {
        if( I._imageView )
        {
            m_dispatcher.device.destroyImageView(I._imageView);
            I._imageView = vk::ImageView();
        }
        if( I._image)
        {
            m_dispatcher.destroy(I._image);
            I._image = vk::Image();
        }

        if( I._imageMemory)
        {
            m_dispatcher.destroy(I._imageMemory);
            I._imageMemory = vk::DeviceMemory();
        }
    }

    void _createImage(ImageData & I)
    {
        vk::ImageCreateInfo create_info;
        create_info.imageType     = vk::ImageType::e2D;// VK_IMAGE_TYPE_2D;
        create_info.extent.width  = I._extent.width;
        create_info.extent.height = I._extent.height;
        create_info.extent.depth  = 1;
        create_info.mipLevels     = 1;
        create_info.arrayLayers   = 1;
        create_info.format        = I.format;
        create_info.tiling        = vk::ImageTiling::eOptimal;
        create_info.initialLayout = vk::ImageLayout::eUndefined;
        create_info.usage         = I._usage;
        create_info.samples       = vk::SampleCountFlagBits::e1;
        create_info.sharingMode   = vk::SharingMode::eExclusive;

        I._image = m_dispatcher.create(create_info);
        I._imageMemory = m_dispatcher.allocateMemoryForImage(I._image, vk::MemoryPropertyFlagBits::eDeviceLocal);

        m_dispatcher.device.bindImageMemory(I._image, I._imageMemory, 0);

        vk::ImageViewCreateInfo C;

        C.image = I._image;
        C.viewType = vk::ImageViewType::e2D;
        C.format                          = I.format;
        C.subresourceRange.aspectMask     = vk::ImageAspectFlagBits::eColor;
        switch(C.format)
        {
            case vk::Format::eD16Unorm:
            case vk::Format::eD32Sfloat:
            case vk::Format::eD16UnormS8Uint:
            case vk::Format::eD24UnormS8Uint:
            case vk::Format::eD32SfloatS8Uint:
                C.subresourceRange.aspectMask = vk::ImageAspectFlagBits::eDepth;
                break;
            default:
                C.subresourceRange.aspectMask = vk::ImageAspectFlagBits::eColor;
            break;
        }
        C.subresourceRange.baseMipLevel   = 0;
        C.subresourceRange.levelCount     = 1;
        C.subresourceRange.baseArrayLayer = 0;
        C.subresourceRange.layerCount     = 1;

        I._imageView = m_dispatcher.device.createImageView(C);
        if( !I._imageView)
            throw std::runtime_error("Failed to create Image view");

        std::cout << "Image Created: " << I._extent.width << "x" << I._extent.height << "  format: " << to_string(I.format) << std::endl;
    }

    void _createSampledImage(SampledImage & S)
    {
        vk::SamplerCreateInfo ci;
        ci.magFilter               =  vk::Filter::eLinear ;
        ci.minFilter               =  vk::Filter::eLinear ;
        ci.mipmapMode              =  vk::SamplerMipmapMode::eNearest ;
        ci.addressModeU            =  vk::SamplerAddressMode::eRepeat ;
        ci.addressModeV            =  vk::SamplerAddressMode::eRepeat ;
        ci.addressModeW            =  vk::SamplerAddressMode::eRepeat ;
        ci.mipLodBias              =  0.0f  ;
        ci.anisotropyEnable        =  VK_FALSE;
        ci.maxAnisotropy           =  1 ;
        ci.compareEnable           =  VK_FALSE ;
        ci.compareOp               =  vk::CompareOp::eAlways ;
        ci.minLod                  =  0 ;
        ci.maxLod                  =  static_cast<float>(1) ;
        ci.borderColor             =  vk::BorderColor::eIntOpaqueBlack ;
        ci.unnormalizedCoordinates =  VK_FALSE ;

        S._sampler = m_dispatcher.device.createSampler(ci);
        if( !S._sampler )
            throw std::runtime_error("Failed to create sampler");
    }

    void _createFramebuffer(RenderPassData & rp)
    {
        vk::FramebufferCreateInfo c;

        // all image sizes need to be the same

        auto e = images.at( attachments.at(rp.colorAttachmentIndex.front()).imageIndex)._extent;


        c.setWidth( e.width );
        c.setHeight( e.height );
        c.setLayers(1);
        c.setRenderPass(rp._renderPass);

        std::vector<vk::ImageView> views;
        for(auto a : rp.colorAttachmentIndex)
        {
            views.push_back( images.at( attachments.at(a).imageIndex )._imageView );
        }
        if( rp.depthAttachmentIndex )
        {
            views.push_back( images.at( attachments.at(*rp.depthAttachmentIndex).imageIndex )._imageView );
        }
        c.setPAttachments(views.data());
        c.setAttachmentCount( static_cast<uint32_t>(views.size()) );

        rp._frameBuffer = m_dispatcher.device.createFramebuffer(c);
        if( !rp._frameBuffer)
            throw std::runtime_error("Failed to create framebuffer");
    }

    void _createRenderPass(RenderPassData & rp)
    {
        std::vector<vk::AttachmentDescription> attachmentDescription;
        std::vector<vk::AttachmentReference>   colorReferences;
        std::vector<vk::SubpassDependency>     subpassDependencies;
        vk::SubpassDescription                 subpassDescription;

        rp.clearValues.clear();
        {
            for(auto i : rp.colorAttachmentIndex)
            {
                vk::AttachmentDescription a;
                // Color attachment
                a.format         = images.at( attachments.at(i).imageIndex ).format;
                a.samples        = vk::SampleCountFlagBits::e1;//VK_SAMPLE_COUNT_1_BIT;									// We don't use multi sampling in this example
                a.loadOp         = vk::AttachmentLoadOp::eClear;//VK_ATTACHMENT_LOAD_OP_CLEAR;							// Clear this attachment at the start of the render pass
                a.storeOp        = vk::AttachmentStoreOp::eStore;//VK_ATTACHMENT_STORE_OP_STORE;							// Keep it's contents after the render pass is finished (for displaying it)
                a.stencilLoadOp  = vk::AttachmentLoadOp::eDontCare;//VK_ATTACHMENT_LOAD_OP_DONT_CARE;					// We don't use stencil, so don't care for load
                a.stencilStoreOp = vk::AttachmentStoreOp::eDontCare;// VK_ATTACHMENT_STORE_OP_DONT_CARE;				// Same for store
                a.initialLayout  = vk::ImageLayout::eUndefined;//VK_IMAGE_LAYOUT_UNDEFINED;						// Layout at render pass start. Initial doesn't matter, so we use undefined

                a.finalLayout    = attachments.at(i)._finalLayout;// vk::ImageLayout::eShaderReadOnlyOptimal;//VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;					// Layout to which the attachment is transitioned when the render pass is finished

                attachmentDescription.push_back(a);

                // Setup attachment references
                vk::AttachmentReference colorReference;
                colorReference.attachment = static_cast<uint32_t>(attachmentDescription.size()-1);													// Attachment 0 is color
                colorReference.layout     = vk::ImageLayout::eColorAttachmentOptimal;// VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;				// Attachment layout used as color during the subpass
                colorReferences.push_back(colorReference);

                rp.clearValues.push_back( vk::ClearColorValue( std::array<float,4>( {0.0f,1.0f,0.0f,1.0f} )));
            }


            if( rp.depthAttachmentIndex)
            {
                vk::AttachmentDescription a;
                // Depth Attachment
                a.format         = images.at( attachments.at( *rp.depthAttachmentIndex ).imageIndex).format;
                a.samples        = vk::SampleCountFlagBits::e1;//VK_SAMPLE_COUNT_1_BIT;									// We don't use multi sampling in this example
                a.loadOp         = vk::AttachmentLoadOp::eClear;//VK_ATTACHMENT_LOAD_OP_CLEAR;							// Clear this attachment at the start of the render pass
                a.storeOp        = vk::AttachmentStoreOp::eStore;//VK_ATTACHMENT_STORE_OP_STORE;							// Keep it's contents after the render pass is finished (for displaying it)
                a.stencilLoadOp  = vk::AttachmentLoadOp::eDontCare;//VK_ATTACHMENT_LOAD_OP_DONT_CARE;					// We don't use stencil, so don't care for load
                a.stencilStoreOp = vk::AttachmentStoreOp::eDontCare;// VK_ATTACHMENT_STORE_OP_DONT_CARE;				// Same for store
                a.initialLayout  = vk::ImageLayout::eUndefined;//VK_IMAGE_LAYOUT_UNDEFINED;						// Layout at render pass start. Initial doesn't matter, so we use undefined

                a.finalLayout    = attachments.at( *rp.depthAttachmentIndex)._finalLayout;//vk::ImageLayout::eShaderReadOnlyOptimal;//VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;					// Layout to which the attachment is transitioned when the render pass is finished

                attachmentDescription.push_back(a);

                // Setup attachment references
                vk::AttachmentReference colorReference;
                colorReference.attachment = static_cast<uint32_t>(attachmentDescription.size()-1);													// Attachment 0 is color
                colorReference.layout     = vk::ImageLayout::eDepthStencilAttachmentOptimal;// VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;				// Attachment layout used as color during the subpass
                colorReferences.push_back(colorReference);

                rp.clearValues.push_back( vk::ClearDepthStencilValue(1.0f,0.0f));
            }
        }


        {
            //============== Default ===========================
            // only create one dependency.
            subpassDependencies.resize(2);
            // First dependency at the start of the renderpass
            // Does the transition from final to initial layout
            subpassDependencies[0].srcSubpass    = VK_SUBPASS_EXTERNAL;								// Producer of the dependency
            subpassDependencies[0].dstSubpass    = 0;													// Consumer is our single subpass that will wait for the execution depdendency
            subpassDependencies[0].srcStageMask  = vk::PipelineStageFlagBits::eBottomOfPipe;// VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
            subpassDependencies[0].dstStageMask  = vk::PipelineStageFlagBits::eColorAttachmentOutput;// VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
            subpassDependencies[0].srcAccessMask = vk::AccessFlagBits::eMemoryRead;// VK_ACCESS_MEMORY_READ_BIT;
            subpassDependencies[0].dstAccessMask = vk::AccessFlagBits::eColorAttachmentRead | vk::AccessFlagBits::eColorAttachmentWrite;// VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
            subpassDependencies[0].dependencyFlags = vk::DependencyFlagBits::eByRegion;// VK_DEPENDENCY_BY_REGION_BIT;

            // Second dependency at the end the renderpass
            // Does the transition from the initial to the final layout
            subpassDependencies[1].srcSubpass      = 0;													// Producer of the dependency is our single subpass
            subpassDependencies[1].dstSubpass      = VK_SUBPASS_EXTERNAL;								// Consumer are all commands outside of the renderpass
            subpassDependencies[1].srcStageMask    = vk::PipelineStageFlagBits::eColorAttachmentOutput;//VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
            subpassDependencies[1].dstStageMask    = vk::PipelineStageFlagBits::eBottomOfPipe;//VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
            subpassDependencies[1].srcAccessMask   = vk::AccessFlagBits::eColorAttachmentRead | vk::AccessFlagBits::eColorAttachmentWrite;
            subpassDependencies[1].dstAccessMask   = vk::AccessFlagBits::eMemoryRead;
            subpassDependencies[1].dependencyFlags = vk::DependencyFlagBits::eByRegion;

            subpassDescription.pipelineBindPoint       = vk::PipelineBindPoint::eGraphics;// VK_PIPELINE_BIND_POINT_GRAPHICS;
            subpassDescription.pColorAttachments       = colorReferences.data();
            subpassDescription.colorAttachmentCount    = static_cast<uint32_t>(colorReferences.size());

            subpassDescription.pDepthStencilAttachment = nullptr;
            if( colorReferences.back().layout == vk::ImageLayout::eDepthStencilAttachmentOptimal)
            {
                subpassDescription.pDepthStencilAttachment = &colorReferences.back();
                subpassDescription.colorAttachmentCount--;
            }
        }

        vk::RenderPassCreateInfo renderPassInfo;
        renderPassInfo.attachmentCount = static_cast<uint32_t>(attachmentDescription.size());		// Number of attachments used by this render pass
        renderPassInfo.pAttachments    = attachmentDescription.data();								// Descriptions of the attachments used by the render pass
        renderPassInfo.subpassCount    = 1;												// We only use one subpass in this example
        renderPassInfo.pSubpasses      = &subpassDescription;								// Description of that subpass
        renderPassInfo.dependencyCount = static_cast<uint32_t>(subpassDependencies.size());	// Number of subpass dependencies
        renderPassInfo.pDependencies   = subpassDependencies.data();

        rp._renderPass = m_dispatcher.device.createRenderPass(renderPassInfo);

        _createFramebuffer(rp);
    }


    void _destroyRenderPass(RenderPassData & rp)
    {
        if( rp._frameBuffer )
        {
            m_dispatcher.device.destroyFramebuffer(rp._frameBuffer);
            rp._frameBuffer = vk::Framebuffer();
        }

        if( rp._renderPass )
        {
            m_dispatcher.device.destroyRenderPass(rp._renderPass);
            rp._renderPass = vk::RenderPass();
        }
    }


    struct FrameGraphInitInfo
    {
        uint32_t        concurrentThreads=1;
        uint32_t        concurrentFrames;
        uint32_t        frameIndex;
        vk::Image       swapChainImage;
        vk::ImageView   swapChainImageView;
        vk::Framebuffer swapChainFramebuffer;
    };

    /**
     * @brief initialize
     * @param I
     *
     * You need to call this once to indicate to the
     * frame graph which resources to initialize and
     * how many threads to run.
     */
    void initialize(FrameGraphInitInfo const I)
    {
        (void)I;
    }



    void finalize()
    {
        for(auto & c : attachments)
        {
            auto format = images.at( c.imageIndex ).format;
            switch(format)
            {
                case vk::Format::eD16Unorm:
                case vk::Format::eD32Sfloat:
                case vk::Format::eD16UnormS8Uint:
                case vk::Format::eD24UnormS8Uint:
                case vk::Format::eD32SfloatS8Uint:
                    images.at( c.imageIndex )._usage |= vk::ImageUsageFlagBits::eDepthStencilAttachment;
                    break;
                default:
                    images.at( c.imageIndex )._usage |= vk::ImageUsageFlagBits::eColorAttachment;
                break;
            }
        }

        size_t ri=0;
        for(auto & r : renderPasses)
        {
            for(auto c : r.colorAttachmentIndex)
            {
                attachments.at(c)._renderPassWriter = ri;
            }
            if( r.depthAttachmentIndex.has_value())
            {
                attachments.at(*r.depthAttachmentIndex )._renderPassWriter = ri;
            }
            ++ri;
        }
        for(auto & s : samplers)
        {
            // all sampled images need to ahve the eSampled flag set
            images.at( attachments.at(s.attachmentIndex).imageIndex )._usage |= vk::ImageUsageFlagBits::eSampled;

            attachments.at(s.attachmentIndex)._finalLayout = vk::ImageLayout::eShaderReadOnlyOptimal;
        }

        validate();
    }

    void validate()
    {
        std::set< std::string > uniqueNames;
        uint32_t ri=0;
        for(auto & r : renderPasses)
        {
            if( r.colorAttachmentIndex.size() ==0)
            {
                throw std::runtime_error("RenderPass, " + r.name + ", has 0 color attachments.");
            }
            if( uniqueNames.count(r.name) > 0)
            {
                throw std::runtime_error("RenderPass, " + r.name + ", Is not a unique name.");
            }
            uniqueNames.insert(r.name);
            ++ri;
        }
        if( uniqueNames.count(swapchain.name) > 0)
        {
            throw std::runtime_error("Swapchain pass, " + swapchain.name + ", Is not a unique name.");
        }
    }

    struct ExecuteInfo
    {
        vk::CommandBuffer           commandBuffer;
        uint32_t                    swapchainFrameIndex;
        vk::Format                  swapchainFormat;
        vk::RenderPass              swapchainRenderPass;
        vk::Image                   swapchainImage;
        vk::ImageView               swapchainImageView;
        vk::Framebuffer             swapchainFramebuffer;
        vk::Extent2D                swapchainExtent;
        std::vector<vk::ClearValue> swapchainClearValues;
    };

    void execute( FrameGraphExecutor & E, ExecuteInfo info)
    {
        auto order = getOrder();

        for(auto i : order)
        {
            auto & rp = renderPasses.at(i);

            if( rp._toDestroy)
            {
                FrameGraphExecutor::RenderPassDestroy RPD;
                RPD.renderPassName = rp.name;
                E.destroy(RPD);
                rp._toDestroy=false;
                rp._initialized=false;
            }

            // create the renderpass and the framebuffers
            if( !rp._renderPass)
            {
                _createRenderPass(rp);
                rp._toInit = true;
            }

            if( rp._toInit )
            {
                rp._toInit = false;

                FrameGraphExecutor::RenderPassInit RPI;
                RPI.renderPassName = rp.name;
                RPI.renderPass = rp._renderPass;
                for(auto inputSamplers : rp.inputSamplers)
                {
                    auto & smp = samplers.at(inputSamplers);
                    auto & ath =  attachments.at(smp.attachmentIndex);
                    auto & img = images.at( ath.imageIndex);

                    FrameGraphExecutor::RenderPassInputAttachment rr;
                    rr.name   = img.name;
                    rr.view   = img._imageView;
                    rr.image  = img._image;
                    rr.format = img.format;
                    rr.extent = img._extent;
                    rr.sampler = smp._sampler;

                    RPI.inputSampledImages.emplace_back(rr);
                }

                E.init(RPI);
                rp._initialized = true;
            }
            {
                FrameGraphExecutor::RenderPassWrite   RPW;

                RPW.renderPassName    = rp.name;
                RPW.clearValues       = rp.clearValues;
                RPW.frameIndex        = info.swapchainFrameIndex;
                RPW.frameBuffer       = rp._frameBuffer;
                RPW.renderPass        = rp._renderPass;

                RPW.frameBufferExtent = images.at( attachments.at(rp.colorAttachmentIndex.at(0)).imageIndex )._extent;

                RPW.cmd = info.commandBuffer;
                RPW.renderPassBeginInfo.clearValueCount   = static_cast<uint32_t>(RPW.clearValues.size());
                RPW.renderPassBeginInfo.pClearValues      = RPW.clearValues.data();
                RPW.renderPassBeginInfo.framebuffer       = RPW.frameBuffer;
                RPW.renderPassBeginInfo.renderPass        = RPW.renderPass;
                RPW.renderPassBeginInfo.renderArea.extent = RPW.frameBufferExtent;

                for(auto inputSamplers : rp.inputSamplers)
                {
                    auto & smp = samplers.at(inputSamplers);
                    auto & ath = attachments.at(smp.attachmentIndex);
                    auto & img = images.at( ath.imageIndex);

                    FrameGraphExecutor::RenderPassInputAttachment rr;
                    rr.name = img.name;
                    rr.view = img._imageView;
                    rr.image = img._image;
                    rr.format = img.format;
                    rr.extent = img._extent;
                    rr.sampler = smp._sampler;

                    RPW.inputSampledImages.emplace_back(rr);
                    //RPW.inputAttachments
                }

                E.write(RPW);
            }
        }

        //=====================================================
        if( swapchain._toDestroy)
        {
            FrameGraphExecutor::RenderPassDestroy RPD;
            RPD.renderPassName = swapchain.name;
            E.destroy(RPD);
            swapchain._toDestroy=false;
            swapchain._initialized=false;
        }

        if( swapchain._toInit )
        {
            swapchain._toInit = false;

            FrameGraphExecutor::RenderPassInit RPI;
            RPI.renderPassName = swapchain.name;

            for(auto inputSamplers : swapchain.inputSamplers)
            {
                auto & smp = samplers.at(inputSamplers);
                auto & ath = attachments.at(smp.attachmentIndex);
                auto & img = images.at( ath.imageIndex);

                FrameGraphExecutor::RenderPassInputAttachment rr;
                rr.name   = img.name;
                rr.view   = img._imageView;
                rr.image  = img._image;
                rr.format = img.format;
                rr.extent = img._extent;
                rr.sampler = smp._sampler;

                #pragma message("Todo: We need to add the depth info here")
                RPI.inputSampledImages.emplace_back(rr);
                //RPW.inputAttachments
            }

            E.init(RPI);
            swapchain._initialized=true;
        }

        {
            FrameGraphExecutor::RenderPassWrite   RPW;

            RPW.renderPassName    = swapchain.name;
            RPW.clearValues       = info.swapchainClearValues;
            RPW.frameIndex        = info.swapchainFrameIndex;
            RPW.frameBuffer       = info.swapchainFramebuffer;// swapchain._swapchainFramebuffers.at(RPW.frameIndex);
            RPW.renderPass        = info.swapchainRenderPass;//swapchain.renderPass;
            RPW.frameBufferExtent = info.swapchainExtent;//swapchain.frameBufferExtent;

            RPW.cmd = info.commandBuffer;
            RPW.renderPassBeginInfo.clearValueCount   = static_cast<uint32_t>(RPW.clearValues.size());
            RPW.renderPassBeginInfo.pClearValues      = RPW.clearValues.data();
            RPW.renderPassBeginInfo.framebuffer       = RPW.frameBuffer;
            RPW.renderPassBeginInfo.renderPass        = RPW.renderPass;
            RPW.renderPassBeginInfo.renderArea.extent = RPW.frameBufferExtent;

            for(auto inputSamplers : swapchain.inputSamplers)
            {
                auto & smp = samplers.at(inputSamplers);
                auto & ath = attachments.at(smp.attachmentIndex);
                auto & img = images.at( ath.imageIndex);;

                FrameGraphExecutor::RenderPassInputAttachment rr;
                rr.name   = img.name;
                rr.view   = img._imageView;
                rr.image  = img._image;
                rr.format = img.format;
                rr.extent = img._extent;
                rr.sampler = smp._sampler;
                RPW.inputSampledImages.emplace_back(rr);
                //RPW.inputAttachments
            }

            E.write(RPW);
        }
        //=================================================================
    }

    //================================
    FrameGraphDispatcher m_dispatcher;

    void fromJson(nlohmann::json const &J)
    {
        if( J.count("images")  )
        {
            images = J.at("images").get< std::vector<ImageData> >();
        }
        if( J.count("attachments")  )
        {
            attachments = J.at("attachments").get< std::vector<ColorAttachment> >();
        }
        if( J.count("samplers")  )
        {
            samplers = J.at("samplers").get< std::vector<SampledImage> >();
        }
        if( J.count("renderPasses")  )
        {
            renderPasses = J.at("renderPasses").get< std::vector<RenderPassData> >();
        }
        swapchain = J.at("swapchain").get< SwaphainData >();

        finalize();
    }

    static nlohmann::json fromJsonFlow2(nlohmann::json J)
    {
        nlohmann::json J2;

        std::map<std::string, size_t> idToNode;
        std::map<std::string, size_t> idToImage;
        std::map<std::string, size_t> idToDepthImage;
        std::map<std::string, size_t> idToAttachment;
        std::map<std::string, size_t> idToDepthAttachment;
        std::map<std::string, size_t> idToSampler;
        std::map<std::string, size_t> idToRenderPass;
        std::map<std::string, size_t> idToSwapchain;

        std::map<std::string, size_t> idToIndex;

        auto & N = J.at("nodes");
        size_t count=0;
        for(auto & n : J.at("nodes"))
        {
            auto id   = n.at("id").get<std::string>();
            auto & model = n.at("model");
            auto name = model.at("name").get<std::string>();

            auto fg = model.count("frameGraph") ? model.at("frameGraph") : nlohmann::json::object();

            idToNode[id] = count;
            count++;

            if( name == COLOR_IMAGE_NAME_STR      )
            {
                J2["images"].push_back(fg);
                idToImage[id] = J2["images"].size()-1;
            }
            if( name == DEPTH_IMAGE_NAME_STR      )
            {
                J2["images"].push_back(fg);
                idToImage[id] = J2["images"].size()-1;
            }
            if( name == COLOR_ATTACHMENT_NAME_STR )
            {
                J2["attachments"].push_back(fg);
                idToAttachment[id] = J2["attachments"].size()-1;
            }
            if( name == DEPTH_ATTACHMENT_NAME_STR )
            {
                J2["attachments"].push_back(fg);
                idToAttachment[id] = J2["attachments"].size()-1;
            }
            if( name == SAMPLED_IMAGE_NAME_STR    )
            {
                J2["samplers"].push_back(fg);
                idToSampler[id] = J2["samplers"].size()-1;
            }
            if( name == SWAPCHAIN_PASS_NAME_STR   )
            {
                J2["swapchain"] = fg;
            }
            if( name == RENDERPASS_NAME_STR       )
            {
                J2["renderPasses"].push_back(fg);
                idToRenderPass[id] = J2["renderPasses"].size()-1;
            }

        }

        for(auto & c : J.at("connections"))
        {
            auto to_id     = c.at("in_id").get<std::string>();
            auto from_id    = c.at("out_id").get<std::string>();
            auto to_index  = c.at("in_index").get<size_t>();
            auto from_index = c.at("out_index").get<size_t>();

            std::string from = N.at( idToNode.at(from_id) ).at("model").at("name");
            std::string to   = N.at( idToNode.at(to_id) ).at("model").at("name");

            if( from == COLOR_IMAGE_NAME_STR && to == COLOR_ATTACHMENT_NAME_STR)
            {
                J2["attachments"][ idToAttachment.at(to_id)]["imageIndex"] = idToImage.at(from_id);
            }
            else if( from == DEPTH_IMAGE_NAME_STR && to == DEPTH_ATTACHMENT_NAME_STR)
            {
                J2["attachments"][ idToAttachment.at(to_id)]["imageIndex"] = idToImage.at(from_id);
            }
            else if( from == COLOR_ATTACHMENT_NAME_STR && to == SAMPLED_IMAGE_NAME_STR)
            {
                J2["samplers"][ idToSampler.at(to_id)]["attachmentIndex"] = idToAttachment.at(from_id);
            }
            else if( from == DEPTH_ATTACHMENT_NAME_STR && to == SAMPLED_IMAGE_NAME_STR)
            {
                J2["samplers"][ idToSampler.at(to_id)]["attachmentIndex"] = idToAttachment.at(from_id);
            }
            else if( from == RENDERPASS_NAME_STR && to == COLOR_ATTACHMENT_NAME_STR)
            {
                J2["renderPasses"][ idToRenderPass.at(from_id)]["colorAttachments"][from_index] = idToAttachment.at(to_id);
            }
            else if( from == RENDERPASS_NAME_STR && to == DEPTH_ATTACHMENT_NAME_STR)
            {
                J2["renderPasses"][ idToRenderPass.at(from_id)]["depthAttachment"] = idToAttachment.at(to_id);
            }
            else if( from == SAMPLED_IMAGE_NAME_STR && to == RENDERPASS_NAME_STR)
            {
                J2["renderPasses"][ idToRenderPass.at(to_id)]["samplers"][to_index] = idToSampler.at(from_id);
            }
            else if( from == SAMPLED_IMAGE_NAME_STR && to == SWAPCHAIN_PASS_NAME_STR)
            {
                J2["swapchain"]["samplers"][to_index] = idToSampler.at(from_id);
            }
        }

        if( J2.count("renderPasses"))
        {
            for(auto & r : J2["renderPasses"])
            {
                if( r.count("samplers"))
                {
                    auto & si = r.at("samplers");
                    si.erase( std::remove_if( si.begin(), si.end(), [](auto & J) { return J.is_null(); } ),
                                si.end());
                }
                if( r.count("colorAttachments"))
                {
                    auto & si = r.at("colorAttachments");
                    si.erase( std::remove_if( si.begin(), si.end(), [](auto & J) { return J.is_null(); } ),
                                si.end());
                }
            }
        }
        if( J2.count("swapchain"))
        {
            auto & r = J2.at("swapchain");
            if( r.count("samplers"))
            {
                auto & si = r.at("samplers");
                si.erase( std::remove_if( si.begin(), si.end(), [](auto & J) { return J.is_null(); } ),
                            si.end());
            }
            if( r.count("colorAttachments"))
            {
                auto & si = r.at("colorAttachments");
                si.erase( std::remove_if( si.begin(), si.end(), [](auto & J) { return J.is_null(); } ),
                            si.end());
            }
        }
        std::cout << J2.dump(4) << std::endl;
        return J2;
    }

    void _recurse(std::vector<size_t> &order, size_t renderPassIndex) const
    {
        order.push_back(renderPassIndex);
        auto & R = renderPasses.at(renderPassIndex);

        for(auto i : R.inputSamplers)
        {
            _recurse( order, attachments.at( samplers.at(i).attachmentIndex )._renderPassWriter);
        }
    }

    std::vector<size_t> getOrder() const
    {
        std::vector<size_t> renderPassOrder;

        for(auto i : swapchain.inputSamplers)
        {
            _recurse(renderPassOrder, attachments.at( samplers.at(i).attachmentIndex )._renderPassWriter );
        }
        std::reverse( renderPassOrder.begin(), renderPassOrder.end());

        auto last = std::unique(renderPassOrder.begin(), renderPassOrder.end());
        renderPassOrder.erase(last, renderPassOrder.end());
        return renderPassOrder;
    }
};


}

#endif
