#ifndef VKA_FRAMEGRAPH_NODE_NAMES_H
#define VKA_FRAMEGRAPH_NODE_NAMES_H

#define COLOR_IMAGE_NAME_STR      "Color Image"
#define DEPTH_IMAGE_NAME_STR      "DepthStencil Image"
#define COLOR_ATTACHMENT_NAME_STR "Color Attachment"
#define DEPTH_ATTACHMENT_NAME_STR "Depth Stencil Attachment"
#define SAMPLED_IMAGE_NAME_STR    "Sampler"
#define SWAPCHAIN_PASS_NAME_STR   "Swapchain Pass"
#define RENDERPASS_NAME_STR       "Render Pass"

#endif
