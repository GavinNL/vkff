#include "catch.hpp"
#include <framegraph/framegraph2.h>
#include <nlohmann/json.hpp>
#include <fstream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>

#include <SDLVulkanWindow.h>
#include <vkb/vkb.h>

struct myExecutor : public vka::FrameGraphExecutor
{
    vkb::Storage m_storage;
    vk::Device   m_device;

    vk::Format m_swapchainFormat;

    vk::Pipeline       m_pipeline;
    vk::PipelineLayout m_pipelineLayout;

    vk::Pipeline       m_postProcessingPipeline;
    vk::PipelineLayout m_postProcessingPipelineLayout;
    vk::DescriptorPool m_descriptorPool;
    vk::DescriptorSet  m_descriptorSet;


    struct firstStagePushConsts
    {
        std::array<float,4> p[3];
        std::array<float,4> c;
    };

    struct postProcessingPushConsts
    {
        std::array<float,2> position;
        std::array<float,2> size;
        std::array<float,2> screenSize;
        std::array<float,2> unused;
    };

public:
    myExecutor(vk::Device d, vk::Format swapchainFormat) : m_device(d)
    {
        // create all objects we need that are not-dependent on the framegraph
        // such as descriptor pools, pipelines, buffers, etc.

        m_swapchainFormat = swapchainFormat;
        vkb::DescriptorPoolCreateInfo2 I;
        I.maxSets = 10;
        I.sizes.emplace_back( vk::DescriptorType::eCombinedImageSampler, 10);
        I.flags = vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet;

        m_descriptorPool = I.create(m_device);

        createPipeline();
        createPipelineFullScreenQuad();
    }

    void destroy()
    {
        m_device.destroyDescriptorPool(m_descriptorPool);
        m_device.destroyPipeline(m_pipeline);
        m_device.destroyPipeline(m_postProcessingPipeline);
        m_storage.destroyAll(m_device);
    }

    void createPipeline()
    {
        vkb::GraphicsPipelineCreateInfo2 PCI;

        // Viewports
        PCI.viewportState.viewports.emplace_back( vk::Viewport(0,0,1024,768,0,1.0f));
        PCI.viewportState.scissors.emplace_back( vk::Rect2D( {0,0}, {1024,768}));

        // vertex inputs
        // no vertex inputs, we will generate the vertex data in the vertex shader
        //uint32_t stride=0+12+24;
        //PCI.setVertexInputAttribute(0,0,vk::Format::eR32G32B32Sfloat,0 );
        //PCI.setVertexInputAttribute(1,1,vk::Format::eR32G32B32Sfloat,12);
        //PCI.setVertexInputAttribute(2,2,vk::Format::eR8G8B8A8Unorm  ,24);
        //
        //PCI.setVertexInputBinding(0,stride, vk::VertexInputRate::eVertex);
        //PCI.setVertexInputBinding(1,stride, vk::VertexInputRate::eVertex);
        //PCI.setVertexInputBinding(2,stride, vk::VertexInputRate::eVertex);

        // shader stages
        PCI.addStage( vk::ShaderStageFlagBits::eVertex,   "main", CMAKE_SOURCE_DIR "/share/shaders/vert.spv");
        PCI.addStage( vk::ShaderStageFlagBits::eFragment, "main", CMAKE_SOURCE_DIR "/share/shaders/frag.spv");

        // Render Pass - we will initialize the pipeline by providing the RenderPassCreateInfo2 struct
        //               and have it auto generate the renderpass for us.
        PCI.renderPass = vkb::RenderPassCreateInfo2::createSimpleRenderPass( {{ vk::Format( vk::Format::eR8G8B8A8Unorm), vk::ImageLayout::eShaderReadOnlyOptimal}},
                                                                                 { vk::Format( vk::Format::eD32Sfloat), vk::ImageLayout::eShaderReadOnlyOptimal});

        // Descriptor sets/Push constants
        PCI.addPushConstantRange(vk::ShaderStageFlagBits::eVertex, 0, 128);

        // single attachment (Swapchain), use default values
        // setColorBlendOp and setBlendEnabled are just
        /// being added for show, these are teh default values
        PCI.addBlendStateAttachment().setColorBlendOp(vk::BlendOp::eAdd)
                                     .setBlendEnable(true);


        auto plr = PCI.create( m_storage, m_device);
        m_pipeline = std::get<0>(plr);
        m_pipelineLayout = std::get<1>(plr);
    }



    void createPipelineFullScreenQuad()
    {
        // This pipeline will be drawing to the swapchain,
        // so we'll need to know what swapchain format is
        // so that we can set the color attachments properly.

        vkb::GraphicsPipelineCreateInfo2 PCI;

        // Viewports
        PCI.viewportState.viewports.emplace_back( vk::Viewport(0,0,1024,768,0,1.0f));
        PCI.viewportState.scissors.emplace_back( vk::Rect2D( {0,0}, {1024,768}));

        // no vertex inputs, generated in the shader

        // shader stages
        PCI.addStage( vk::ShaderStageFlagBits::eVertex,   "main", CMAKE_SOURCE_DIR "/share/shaders/fullscreen_quad.vert.spv");
        PCI.addStage( vk::ShaderStageFlagBits::eFragment, "main", CMAKE_SOURCE_DIR "/share/shaders/fullscreen_quad.frag.spv");

        // Render Pass - we will initialize the pipeline by providing the RenderPassCreateInfo2 struct
        //               and have it auto generate the renderpass for us.
        //             no depth testing
        PCI.renderPass = vkb::RenderPassCreateInfo2::createSimpleRenderPass({{ m_swapchainFormat, vk::ImageLayout::eShaderReadOnlyOptimal}} );

        // Descriptor sets/Push constants
        PCI.newDescriptorSet().addDescriptor(0,vk::DescriptorType::eCombinedImageSampler,1,vk::ShaderStageFlagBits::eFragment);
        PCI.addPushConstantRange(vk::ShaderStageFlagBits::eVertex | vk::ShaderStageFlagBits::eFragment, 0, 4 * 2 * sizeof(float) );

        // single attachment (Swapchain), use default values
        // setColorBlendOp and setBlendEnabled are just
        /// being added for show, these are teh default values
        PCI.addBlendStateAttachment().setColorBlendOp(vk::BlendOp::eAdd)
                                     .setBlendEnable(true);

        PCI.rasterizationState.setCullMode(vk::CullModeFlagBits::eNone);
        auto plr = PCI.create( m_storage, m_device);
        m_postProcessingPipeline = std::get<0>(plr);
        m_postProcessingPipelineLayout = std::get<1>(plr);
    }


    void init(RenderPassInit & C) override
    {
        // The init method is called whenever framegraph.resize() is called.
        // If the swapchain has changed shape, the destroy(RenderPassDestroy&)
        // method will be called first.

        std::cout << "RenderPass Initialized: " << C.renderPassName << std::endl;
        if( C.renderPassName == "firstPass")
        {
            // we dont need to init anything for this.
            // Alternatively, we can use the C.renderPass data
            // to create the pipelines the first time init() is called
        }
        else if( C.renderPassName == "swapchainPass")
        {
            // Since this renderpass requires input textures,
            // we will need to create descriptor sets and update them
            // in this section. This is because whenever the swapchain changes
            // size, its possible that the input textures will have been
            // reallocated.
            REQUIRE( C.inputSampledImages.size()   == 1 );
            REQUIRE( C.inputSampledImages[0].image != vk::Image());
            REQUIRE( C.inputSampledImages[0].view  != vk::ImageView());


            vkb::DescriptorSetLayoutCreateInfo2 L;
            L.addDescriptor(0, vk::DescriptorType::eCombinedImageSampler, 1, vk::ShaderStageFlagBits::eFragment);

            // instead of creating the layout first, we can simply tell the createINfo struct
            // to create the layout in the storage, and allocate it from the pool
            m_descriptorSet = L.allocateFromPool( m_storage, m_descriptorPool, m_device );

            vkb::DescriptorSetUpdater writer;

            writer.updateImageDescriptor(m_descriptorSet,
                               0,
                               0,
                               vk::DescriptorType::eCombinedImageSampler,
                               {
                                   {C.inputSampledImages[0].sampler, C.inputSampledImages[0].view, vk::ImageLayout::eShaderReadOnlyOptimal}
                               } );

            writer.create_t([this]( std::vector<vk::WriteDescriptorSet> const & w)
            {
                m_device.updateDescriptorSets( w, nullptr);
            });
        }
    }

    void destroy(RenderPassDestroy & D) override
    {
        std::cout << "RenderPass Destroyed: " << D.renderPassName << std::endl;
        if( D.renderPassName == "swapchainPass")
        {
            // Free the descriptor set that we allocated for this pass.
            m_device.freeDescriptorSets(m_descriptorPool, m_descriptorSet);
        }
    }

    void write(RenderPassWrite & C) override
    {
        // This is where the main rendering happens for each of the
        // render passes. we can determine which renderpass is
        // being called by using the C.renderPassName method.
        if( C.renderPassName == "firstPass")
        {
            REQUIRE( C.inputSampledImages.size() == 0 );

            REQUIRE( C.clearValues.size() == 2);

            REQUIRE( C.renderPassBeginInfo.renderPass  == C.renderPass );
            REQUIRE( C.renderPassBeginInfo.framebuffer == C.frameBuffer );
            REQUIRE( C.renderPassBeginInfo.renderArea.extent == C.frameBufferExtent);
            REQUIRE( C.renderPassBeginInfo.clearValueCount == static_cast<uint32_t>( C.clearValues.size()) );
            REQUIRE( C.renderPassBeginInfo.pClearValues == C.clearValues.data() );

            // Set the clear color value to something other than the default.
            // the renderpassBeginInfo is currently pointing to this
            C.clearValues[0].setColor(  vk::ClearColorValue( std::array<float,4>( {1.0f,0.0f,0.0f,1.0f} ) )  );

            C.cmd.beginRenderPass( C.renderPassBeginInfo, vk::SubpassContents::eInline );

                firstStagePushConsts push;
                push.p[0] = {  0.5f,  0.f, 0.f, 1.f };
                push.p[1] = {  0.f,  0.5f, 0.f, 1.f };
                push.p[2] = {  0.f,  0.f, 0.f, 1.f };
                push.c    = {  0.f,  1.f, 0.f, 1.f };

                // we would write our command buffers here.
                // for this ultra-simple example, we are only going to let
                // the renderpass clear the swapchain image to RED   
                C.cmd.bindPipeline(vk::PipelineBindPoint::eGraphics, m_pipeline);

                C.cmd.pushConstants( m_pipelineLayout, vk::ShaderStageFlagBits::eVertex,0, sizeof(push), &push);
                C.cmd.draw(3, 1,0,1);

            C.cmd.endRenderPass();
        }
        else if( C.renderPassName == "swapchainPass")
        {
            REQUIRE( C.inputSampledImages.size() == 1 );

            REQUIRE( C.inputSampledImages[0].image != vk::Image());
            REQUIRE( C.inputSampledImages[0].view  != vk::ImageView());

            REQUIRE( C.clearValues.size() == 1);
            REQUIRE( C.renderPassBeginInfo.renderPass  == C.renderPass );
            REQUIRE( C.renderPassBeginInfo.framebuffer == C.frameBuffer );
            REQUIRE( C.renderPassBeginInfo.renderArea.extent == C.frameBufferExtent);
            REQUIRE( C.renderPassBeginInfo.clearValueCount == static_cast<uint32_t>( C.clearValues.size()) );
            REQUIRE( C.renderPassBeginInfo.pClearValues == C.clearValues.data() );

            // Set the clear color value to something other than the default.
            // the renderpassBeginInfo is currently pointing to this
            C.clearValues[0].setColor(  vk::ClearColorValue( std::array<float,4>( {1.0f,1.0f,0.0f,1.0f} ) )  );

            C.cmd.beginRenderPass( C.renderPassBeginInfo, vk::SubpassContents::eInline );

            C.cmd.bindDescriptorSets( vk::PipelineBindPoint::eGraphics, m_postProcessingPipelineLayout,0, m_descriptorSet, nullptr);
            C.cmd.bindPipeline(vk::PipelineBindPoint::eGraphics, m_postProcessingPipeline);



            postProcessingPushConsts pushc;
            pushc.size = {2,2};
            pushc.position = {-1,-1};
            pushc.screenSize = { static_cast<float>(C.frameBufferExtent.width), static_cast<float>(C.frameBufferExtent.height) };

            C.cmd.pushConstants( m_postProcessingPipelineLayout,
                                 vk::ShaderStageFlagBits::eVertex | vk::ShaderStageFlagBits::eFragment,
                                 0,
                                 sizeof(postProcessingPushConsts),
                                 &pushc);

            C.cmd.draw(6, 1,0,1);

                // we would write our command buffers here.
                // for this ultra-simple example, we are only going to let
                // the renderpass clear the swapchain image to RED

            C.cmd.endRenderPass();
        }
    }
};



static VKAPI_ATTR VkBool32 VKAPI_CALL VulkanReportFunc(
    VkDebugReportFlagsEXT flags,
    VkDebugReportObjectTypeEXT objType,
    uint64_t obj,
    size_t location,
    int32_t code,
    const char* layerPrefix,
    const char* msg,
    void* userData)
{
    printf("VULKAN VALIDATION: [%s] %s\n", layerPrefix, msg);
    //throw std::runtime_error( msg );
    return VK_FALSE;
}


SCENARIO( " Scenario 1" )
{
    SDL_Init(SDL_INIT_EVERYTHING);
    auto window = new SDLVulkanWindow();

    // 1. create the window
    window->createWindow("Simple Deferred", SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED, 1024,768);

    // 2. initialize the vulkan instance
    SDLVulkanWindow::InitilizationInfo info;
    info.callback = VulkanReportFunc;
    window->createVulkanInstance( info);

    // 3. Create the following objects:
    //    instance, physical device, device, graphics/present queues,
    //    swap chain, depth buffer, render pass and framebuffers
    SDLVulkanWindow::SurfaceInitilizationInfo sii;
    sii.depthFormat = VkFormat::VK_FORMAT_UNDEFINED;
    window->initSurface(sii);

    std::ifstream i( CMAKE_SOURCE_DIR "/samples/simpleDeferred.json");
    if( !i )
    {
        throw std::runtime_error("Failed to open flow file");
    }
    nlohmann::json J;
    i >> J;

    vka::FrameGraph2 framegraph;

    framegraph.fromJson(J);

    // There are no render passes other than the swapchain pass
    REQUIRE( framegraph.renderPasses.size()  == 1);

    REQUIRE( framegraph.images.size() == 2);

    REQUIRE( framegraph.images[0].format == vk::Format::eR8G8B8A8Unorm);
    REQUIRE( framegraph.images[1].format == vk::Format::eD32Sfloat);

    REQUIRE( framegraph.attachments.size() == 2);


    REQUIRE( framegraph.swapchain.name == "swapchainPass" );
    REQUIRE( framegraph.renderPasses[0].name == "firstPass" );


    vka::FrameGraphDispatcher dispatcher;
    dispatcher.device         = window->getDevice();
    dispatcher.physicalDevice = window->getPhysicalDevice();
    dispatcher.graphicsQueue  = window->getGraphicsQueue();
    dispatcher.swapchain      = window->getSwapchain();
    framegraph.m_dispatcher   = dispatcher;

    // resize the framegraph to the size of the
    // swapchain. This will allocate any internal
    // images which depend on the size of the swapchain (eg: gBuffers)
    auto e = window->getSwapchainExtent();

    vka::FrameGraph2::ResizeInfo r_info;
    r_info.width  = e.width;
    r_info.height = e.height;
    framegraph.resize(r_info);


    // This is the instance of the executor we are going to use
    // for the frame graph. You would store all your
    // vulkan objects that you create in your implementation.
    myExecutor exe(window->getDevice(), vk::Format(window->getSwapchainFormat()) );

    bool running=true;
    while(running)
    {
        SDL_Event event;
        bool resize=false;
        while (SDL_PollEvent(&event))
        {
            if (event.type == SDL_QUIT)
            {
                running = false;
            }
            else if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_RESIZED
                    /*&& event.window.windowID == SDL_GetWindowID( window->getSDLWindow()) */ )
            {
                resize=true;
            }
        }
        if(resize)
        {
            window->rebuildSwapchain();

            auto e = window->getSwapchainExtent();

            vka::FrameGraph2::ResizeInfo r_info;
            r_info.width = e.width;
            r_info.height = e.height;
            framegraph.resize(r_info);
        }

        auto frame = window->acquireNextFrame();


        //===========================================================================
        // Set up the execute info struct. This tells the framegraph
        // information about the swapchain image that it will render to.
        //
        // The framegraph does not manage the swapchain. This is up to the user
        // and sometimes the vulkan window provided. For example, the Qt vulkan window
        // manages the swapchain and the renderpass for you.
        //
        // So to initiate the framegraph, you must provide information about the
        // the swapchain and which framebuffer to render to.
        //===========================================================================
        vka::FrameGraph2::ExecuteInfo eInfo;
        eInfo.commandBuffer        = frame.commandBuffer;

        // information about the swapchain
        eInfo.swapchainFrameIndex  = frame.swapchainIndex;
        eInfo.swapchainRenderPass  = frame.renderPass;
        eInfo.swapchainImageView   = frame.swapchainImageView;
        eInfo.swapchainImage       = frame.swapchainImage;
        eInfo.swapchainFramebuffer = frame.framebuffer;
        eInfo.swapchainExtent      = frame.swapchainSize;
        eInfo.swapchainClearValues = { { vk::ClearColorValue( std::array<float,4>{1.f,0.f,0.f,1.0f})  } };
        //===========================================================================
        // Since we are using a single Command buffer, we should
        // call the begin method ourself.

        vk::CommandBufferBeginInfo cbbi;
        cbbi.flags = vk::CommandBufferUsageFlagBits::eOneTimeSubmit;
        vk::CommandBuffer(frame.commandBuffer).begin( cbbi);

            framegraph.execute(exe, eInfo);

        vk::CommandBuffer(frame.commandBuffer).end();

        window->submitFrame(frame);
        window->presentFrame(frame);
        window->waitForPresent();
    }

    exe.destroy();

    framegraph.free();

    delete window;
    SDL_Quit();

}

