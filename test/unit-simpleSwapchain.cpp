#include "catch.hpp"
#include <framegraph/framegraph2.h>
#include <nlohmann/json.hpp>
#include <fstream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>

#include <SDLVulkanWindow.h>

vka::FrameGraph2::ExecuteInfo randomData;
struct myExecutor : public vka::FrameGraphExecutor
{

    // FrameGraphExecutor interface
public:
    void init(RenderPassInit & C) override
    {
        if( C.renderPassName == "swapchainPass")
        {
            // Here we would normally create resources
            // which depend on the input textures. For example
            // if we resize the framegraph, any input textures which
            // depend on the size of the swapchain images will be
            // recreated in the new size. so any descriptor sets
            // would become invalidated.
        }
    }

    void destroy(RenderPassDestroy & D) override
    {
        if( D.renderPassName == "swapchainPass")
        {
            // we dont need to do anything here since
            // we did not create any reesources that depend on the
            // swapchain.
        }
    }

    void write(RenderPassWrite & C) override
    {
        REQUIRE( C.inputSampledImages.size() == 0 );
        REQUIRE( C.clearValues.size() == 1);
        REQUIRE( C.renderPassBeginInfo.renderPass  == C.renderPass );
        REQUIRE( C.renderPassBeginInfo.framebuffer == C.frameBuffer );
        REQUIRE( C.renderPassBeginInfo.renderArea.extent == C.frameBufferExtent);
        REQUIRE( C.renderPassBeginInfo.clearValueCount == static_cast<uint32_t>( C.clearValues.size()) );
        REQUIRE( C.renderPassBeginInfo.pClearValues == C.clearValues.data() );

        if( C.renderPassName == "swapchainPass")
        {
            // Set the clear color value to something other than the default.
            // the renderpassBeginInfo is currently pointing to this
            C.clearValues[0].setColor(  vk::ClearColorValue( std::array<float,4>( {1.0f,1.0f,0.0f,1.0f} ) )  );

            C.cmd.beginRenderPass( C.renderPassBeginInfo, vk::SubpassContents::eInline );

                // we would write our command buffers here.
                // for this ultra-simple example, we are only going to let
                // the renderpass clear the swapchain image to RED

            C.cmd.endRenderPass();
        }
    }
};



static VKAPI_ATTR VkBool32 VKAPI_CALL VulkanReportFunc(
    VkDebugReportFlagsEXT flags,
    VkDebugReportObjectTypeEXT objType,
    uint64_t obj,
    size_t location,
    int32_t code,
    const char* layerPrefix,
    const char* msg,
    void* userData)
{
    printf("VULKAN VALIDATION: [%s] %s\n", layerPrefix, msg);
    //throw std::runtime_error( msg );
    return VK_FALSE;
}


SCENARIO( " Scenario 1" )
{
    SDL_Init(SDL_INIT_EVERYTHING);
    auto window = new SDLVulkanWindow();

    // 1. create the window
    window->createWindow("Simple Deferred", SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED, 1024,768);

    // 2. initialize the vulkan instance
    SDLVulkanWindow::InitilizationInfo info;
    info.callback = VulkanReportFunc;
    window->createVulkanInstance( info);

    // 3. Create the following objects:
    //    instance, physical device, device, graphics/present queues,
    //    swap chain, depth buffer, render pass and framebuffers
    SDLVulkanWindow::SurfaceInitilizationInfo sii;
    sii.depthFormat = VkFormat::VK_FORMAT_UNDEFINED;
    window->initSurface(sii);

    std::ifstream i( CMAKE_SOURCE_DIR "/samples/simpleSwapchain.flow");
    if( !i )
    {
        throw std::runtime_error("Failed to open flow file");
    }
    nlohmann::json J;
    i >> J;

    vka::FrameGraph2 framegraph;

    framegraph.fromJson(framegraph.fromJsonFlow2(J));

    // There are no render passes other than the swapchain pass
    REQUIRE( framegraph.renderPasses.size()  == 0);
    REQUIRE( framegraph.attachments.size() == 0);
    REQUIRE( framegraph.images.size() == 0);
    REQUIRE( framegraph.swapchain.name == "swapchainPass" );

    vka::FrameGraphDispatcher dispatcher;
    dispatcher.device         = window->getDevice();
    dispatcher.physicalDevice = window->getPhysicalDevice();
    dispatcher.graphicsQueue  = window->getGraphicsQueue();
    dispatcher.swapchain      = window->getSwapchain();
    framegraph.m_dispatcher   = dispatcher;

    // resize the framegraph to the size of the
    // swapchain. This will allocate any internal
    // images which depend on the size of the swapchain (eg: gBuffers)
    auto e = window->getSwapchainExtent();

    vka::FrameGraph2::ResizeInfo r_info;
    r_info.width  = e.width;
    r_info.height = e.height;
    framegraph.resize(r_info);


    // This is the instance of the executor we are going to use
    // for the frame graph. You would store all your
    // vulkan objects that you create in your implementation.
    myExecutor exe;

    bool running=true;
    while(running)
    {
        SDL_Event event;
        bool resize=false;
        while (SDL_PollEvent(&event))
        {
            if (event.type == SDL_QUIT)
            {
                running = false;
            }
            else if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_RESIZED
                    /*&& event.window.windowID == SDL_GetWindowID( window->getSDLWindow()) */ )
            {
                resize=true;
            }
        }
        if(resize)
        {
            window->rebuildSwapchain();

            auto e = window->getSwapchainExtent();

            vka::FrameGraph2::ResizeInfo r_info;
            r_info.width = e.width;
            r_info.height = e.height;
            framegraph.resize(r_info);
        }

        auto frame = window->acquireNextFrame();


        //===========================================================================
        // Set up the execute info struct. This tells the framegraph
        // information about the swapchain image that it will render to.
        //
        // The framegraph does not manage the swapchain. This is up to the user
        // and sometimes the vulkan window provided. For example, the Qt vulkan window
        // manages the swapchain and the renderpass for you.
        //
        // So to initiate the framegraph, you must provide information about the
        // the swapchain and which framebuffer to render to.
        //===========================================================================
        vka::FrameGraph2::ExecuteInfo eInfo;
        eInfo.commandBuffer        = frame.commandBuffer;

        // information about the swapchain
        eInfo.swapchainFrameIndex  = frame.swapchainIndex;
        eInfo.swapchainRenderPass  = frame.renderPass;
        eInfo.swapchainImageView   = frame.swapchainImageView;
        eInfo.swapchainImage       = frame.swapchainImage;
        eInfo.swapchainFramebuffer = frame.framebuffer;
        eInfo.swapchainExtent      = frame.swapchainSize;
        eInfo.swapchainClearValues = { { vk::ClearColorValue( std::array<float,4>{1.f,0.f,0.f,1.0f})  } };
        //===========================================================================
        // Since we are using a single Command buffer, we should
        // call the begin method ourself.

        vk::CommandBufferBeginInfo cbbi;
        cbbi.flags = vk::CommandBufferUsageFlagBits::eOneTimeSubmit;
        vk::CommandBuffer(frame.commandBuffer).begin( cbbi);

            framegraph.execute(exe, eInfo);

        vk::CommandBuffer(frame.commandBuffer).end();

        window->submitFrame(frame);
        window->presentFrame(frame);
        window->waitForPresent();
    }

    framegraph.free();

    delete window;
    SDL_Quit();

}

