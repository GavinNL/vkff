[requires]
spdlog/1.5.0
sdl2/2.0.9@bincrafters/stable
nlohmann_json/3.7.3

[generators]
cmake
cmake_paths
cmake_find_package

